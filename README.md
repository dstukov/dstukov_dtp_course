# Doubletapp backend course

## Features

1. [Fully typed](https://github.com/typeddjango/django-stubs) `Django`
2. Async ORM (`Django 4.1+`)
3. [aiogram3](https://docs.aiogram.dev/en/dev-3.x/) as Telegram Bot API

## Installation

1. #### Clone:
   * `git clone https://gitlab.com/dstukov/doubletapp_course`

2. #### Install Requirements
   * `pipenv install`

3. #### Change the configuration:
   * Rename `.env.example` to `.env`
   * Insert your values or delete unnecessary fields

4. #### Make migrations:
   * `make makemigrations` or `python manage.py makemigrations`
   * `make migrate` or `python manage.py migrate`

5. #### Run bot:
   * `make run-bot` or ```python -m src.app.internal.bot```

6. #### Run server (Django):
   * Run server (local): `make dev` or `python manage.py runserver localhost:8000`
