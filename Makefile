include .env

run-bot:
	python -m src.app.internal.bot

migrate:
	docker-compose run --rm web python manage.py migrate $(if $m, api $m,)

makemigrations:
	docker-compose run --rm web python manage.py makemigrations
	sudo chown -R ${USER} src/app/migrations/

createsuperuser:
	docker-compose run --rm web python manage.py createsuperuser --no-input

collectstatic:
	docker-compose run --rm web python manage.py collectstatic --no-input

build:
	docker-compose build

test:
	docker-compose run --rm web pytest src/tests

up:
	docker-compose up -d

down:
	docker-compose down

push:
	docker push ${IMAGE}

pull:
	docker pull ${IMAGE}

command:
	sudo docker-compose run --rm web python manage.py ${c}

shell:
	sudo docker-compose run --rm web python manage.py shell

debug:
	sudo docker-compose run --rm web python manage.py debug

piplock:
	pipenv install
	sudo chown -R ${USER} Pipfile.lock

lint:
	docker-compose run --rm web isort .
	docker-compose run --rm web flake8 --config setup.cfg
	docker-compose run --rm web black --config pyproject.toml .

check_lint:
	docker-compose run --rm web isort --check --diff .
	docker-compose run --rm web flake8 --config setup.cfg
	docker-compose run --rm web black --check --config pyproject.toml .
