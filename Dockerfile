FROM python:3.10-alpine

ENV LANG=C.UTF-8
ENV LC_ALL=C.UTF-8
ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONFAULTHANDLER=1
ENV PYTHONUNBUFFERED=1
ENV PIPENV_DEV=1

WORKDIR /app

COPY Pipfile Pipfile.lock ./

RUN python -m pip install --upgrade pip && \
    pip install pipenv && \
    apk update && apk add postgresql-dev gcc python3-dev musl-dev && \
    pipenv install --dev --system --deploy

COPY . .

CMD ["python3", "-m", "src.app.internal.bot"]