from django.contrib import admin
from django.urls import URLPattern, URLResolver, path

from src.app.internal.handlers.rest.handlers import api

urlpatterns: list[URLResolver | URLPattern] = [
    path("admin/", admin.site.urls),
    path("api/", api.urls),
]
