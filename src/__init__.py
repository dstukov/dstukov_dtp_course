import os

import django
import django_stubs_ext

django_stubs_ext.monkeypatch()

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "src.config.settings")
os.environ["DJANGO_ALLOW_ASYNC_UNSAFE"] = "true"
django.setup()
