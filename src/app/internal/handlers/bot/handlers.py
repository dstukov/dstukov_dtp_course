import io

from aiogram import Bot, F, Router
from aiogram.filters import Command, CommandObject
from aiogram.fsm.context import FSMContext
from aiogram.fsm.state import State, StatesGroup
from aiogram.types import KeyboardButton, Message, ReplyKeyboardMarkup, ReplyKeyboardRemove
from django.core.exceptions import ObjectDoesNotExist
from django.utils import timezone

from src.app.internal.auth.db.repositories import IssuedTokenRepository
from src.app.internal.auth.domain.services import AuthService
from src.app.internal.bank.db.repositories import PaymentAccountRepository, PaymentCardRepository, TransactionRepository
from src.app.internal.bank.domain.services import (
    AccountStatementService,
    PaymentAccountService,
    PaymentCardService,
    TransactionService,
)
from src.app.internal.middlewares.phone_required_middleware import PhoneRequiredMiddleware
from src.app.internal.postcards.db.repositories import PostcardRepository
from src.app.internal.users.db.models import User
from src.app.internal.users.db.repositories import FavoriteRepository, UserRepository
from src.app.internal.users.domain.services import FavoriteService, UserService
from src.app.internal.utils.constants import BOT_COMMANDS, PHONE_REQUIRED_FLAG, BotCommandMeta
from src.app.internal.utils.user_serializer import serialize_user_telegram
from src.config.bot import TG_TOKEN

user_repo = UserRepository()
user_service = UserService(user_repo)
transaction_repo = TransactionRepository()
account_repo = PaymentAccountRepository(transaction_repo, user_repo)
card_repo = PaymentCardRepository()
payment_card_service = PaymentCardService(card_repo)
payment_account_service = PaymentAccountService(account_repo, transaction_repo)
account_statement_service = AccountStatementService(account_repo, card_repo, transaction_repo)
token_repo = IssuedTokenRepository()
auth_service = AuthService(user_repo, token_repo)
favorite_repo = FavoriteRepository(user_repo)
favorite_service = FavoriteService(user_repo, favorite_repo)
transaction_service = TransactionService(transaction_repo)
postcard_repository = PostcardRepository()

router = Router()
router.message.middleware(PhoneRequiredMiddleware())


class LocalState(StatesGroup):
    postcard_photo = State()


@router.message(Command("me"), flags=BOT_COMMANDS["me"]["flags"])
async def send_me_reply(message: Message):
    user = await user_service.get_by_chat_id(message.from_user.id)
    await message.reply(f"<b>Information about user:</b> {serialize_user_telegram(user)}")


@router.message(Command("set_phone"), flags=BOT_COMMANDS["set_phone"]["flags"])
async def send_set_phone_reply(message: Message):
    button = KeyboardButton(text="Set phone number", request_contact=True)
    markup = ReplyKeyboardMarkup(keyboard=[[button]], resize_keyboard=True, one_time_keyboard=True)
    await message.reply("Set phone number", reply_markup=markup)


@router.message(F.content_type.in_({"contact"}))
async def handle_get_contact(message: Message):
    user = await user_service.set_phone(message.from_user.id, message.contact.phone_number)
    await message.answer(f"Save phone number: {user.phone_number}", reply_markup=ReplyKeyboardRemove())


@router.message(Command("start"), flags=BOT_COMMANDS["start"]["flags"])
async def send_start_reply(message: Message):
    user, is_new = await user_service.register_bot_user(
        chat_id=message.from_user.id,
        username=message.from_user.username,
        first_name=message.from_user.first_name,
        last_name=message.from_user.last_name,
    )

    if is_new:
        await message.answer("Welcome.")
    else:
        await message.answer("You are already registered.")

    await send_default_message(message)


@router.message(Command("payment_account_balance"), flags=BOT_COMMANDS["payment_account_balance"]["flags"])
async def send_payment_account_balance_reply(message: Message):
    money = await payment_account_service.get_money_by_chat_id(message.from_user.id)
    cards = await payment_account_service.get_payment_account_cards(message.from_user.id)
    cards_balance = str.join("\n", cards)
    await message.answer(f"Account balance: {money}\n\nCards:\n{cards_balance}")


@router.message(Command("transfer_money"), flags=BOT_COMMANDS["transfer_money"]["flags"])
async def send_transfer_money_reply(message: Message, command: CommandObject, state: FSMContext):
    args = command.args.split(" ")

    if len(args) != 2:
        return await message.answer(f"Incorrect amount of arguments: {len(args)}. Should be 2")

    [money, username_or_account_id] = args

    if not str.isdigit(money):
        return await message.reply(f"Money should consists of digits, but found {money}")

    money = int(money)

    account = await payment_account_service.get_by_chat_id(message.from_user.id)

    if account.money < money:
        return await message.reply(f"Not enough money: need {money - account.money} to success")

    if str.isdigit(username_or_account_id):
        try:
            recipient_account = await payment_account_service.get_by_chat_id(int(username_or_account_id))
        except ObjectDoesNotExist:
            return await message.reply(f"Account with id {username_or_account_id} not found for another user")
    else:
        user = await user_service.get_by_username(username_or_account_id)
        if account.user.id == user.id:
            return await message.reply("Cant send money to itself by name")
        try:
            recipient_account = await payment_account_service.get_by_chat_id(user.chat_id)
        except ObjectDoesNotExist:
            return await message.reply("Another user haven't any account")

    if account == recipient_account:
        return await message.reply("Cant send money to same account")

    transaction = await payment_account_service.transfer_money(account, recipient_account, money)

    await state.set_state(LocalState.postcard_photo)
    await state.set_data({"trans_id": transaction.id})
    return await message.answer("Transfer success. You can add postcard by sending photo now or skip that step")


@router.message(LocalState.postcard_photo)
async def send_postcard_answer(message: Message, state: FSMContext):
    transaction_id = (await state.get_data())["trans_id"]
    await state.clear()
    if message.photo is None:
        return message.answer("No photo found")

    bot = Bot(TG_TOKEN, parse_mode="HTML")
    file_id = message.photo[-1].file_id
    file = await bot.get_file(file_id)

    result: io.BytesIO = await bot.download_file(file.file_path)
    result.seek(0)

    transaction = await transaction_service.get_by_id(transaction_id)
    postcard = postcard_repository.add_postcard(result.read())
    await transaction_service.attach_postcard(transaction, postcard)

    return message.answer("Success")


@router.message(Command("set_favorite"), flags=BOT_COMMANDS["set_favorite"]["flags"])
async def send_set_favorite_reply(message: Message, command: CommandObject):
    id_or_username = command.args

    if str.isdigit(id_or_username):
        await favorite_service.add_favorite_by_chat_id(message.from_user.id, int(id_or_username))
    else:
        await favorite_service.add_favorite_by_username(message.from_user.id, id_or_username)

    await message.answer(f"Add user {id_or_username}")


@router.message(Command("delete_favorite"), flags=BOT_COMMANDS["delete_favorite"]["flags"])
async def send_delete_favorite_reply(message: Message, command: CommandObject):
    id_or_username = command.args

    if str.isdigit(id_or_username):
        await favorite_service.remove_favorite_by_id(message.from_user.id, int(id_or_username))
    else:
        await favorite_service.remove_favorite_by_username(message.from_user.id, id_or_username)

    await message.answer(f"Remove user {id_or_username}")


@router.message(Command("favorites"), flags=BOT_COMMANDS["favorites"]["flags"])
async def send_favorites_reply(message: Message):
    favorites = await favorite_service.get_favorites(message.from_user.id)

    await message.answer(f"{favorites}")


@router.message(Command("get_transaction_users"), flags=BOT_COMMANDS["get_transaction_users"]["flags"])
async def send_get_transaction_users_answer(message: Message):
    transactions = await transaction_service.get_transaction_history(message.from_user.id)
    if len(transactions) == 0:
        return await message.answer("No transactions found")
    else:
        return await message.answer(f"Transactions: {', '.join(transactions)}")


@router.message(Command("get_account_statement"), flags=BOT_COMMANDS["get_account_statement"]["flags"])
async def send_get_account_statement_answer(message: Message):
    previous_30_days = timezone.now() - timezone.timedelta(30)
    statement = await account_statement_service.get_account_statement(message.from_user.id, previous_30_days)

    return await message.answer(f"Account statement:\n{str(statement)}")


@router.message(Command("new_transactions"), flags=BOT_COMMANDS["new_transactions"]["flags"])
async def send_new_transactions_answer(message: Message):
    account = await payment_account_service.get_by_chat_id(message.from_user.id)
    transactions = await transaction_service.get_not_viewed_transactions(account.id)

    if len(transactions) == 0:
        return await message.answer("Empty new transactions")
    else:
        for transaction in transactions:
            if transaction.viewed:
                continue

            description = f"Transaction from {transaction.sender_account.user.username} to {transaction.recipient_account.user.username} with money {transaction.money} at {transaction.date}"
            if transaction.postcard is None:
                await message.answer(description)
            else:
                await message.answer_photo(photo=transaction.postcard.image.url, caption=description)

            await transaction_service.mark_transaction_viewed(transaction)


def _construct_line(item: (str, BotCommandMeta), need: bool):
    need_lock = need and PHONE_REQUIRED_FLAG in (item[1]["flags"])

    return f"{'🔒 ' if need_lock else ''}/{item[0]}: {item[1]['description']}"


@router.message()
async def send_default_message(message: Message):
    user = await user_service.get_by_chat_id(message.from_user.id)

    await message.answer(get_commands(user))


def get_commands(user: User):
    has_blocked_commands = user.phone_number is None or len(user.phone_number) == 0
    blocked_commands_message = "Set phone number to use 🔒 commands"
    commands = str.join("\n", list(map(lambda item: _construct_line(item, has_blocked_commands), BOT_COMMANDS.items())))

    return f"""
My list of commands:
{commands}

{blocked_commands_message if has_blocked_commands else ''}
"""
