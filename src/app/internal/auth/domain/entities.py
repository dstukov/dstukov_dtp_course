from _decimal import Decimal
from ninja import Schema


class JWTTokenSchemaOut(Schema):
    refresh_token: str
    access_token: str


class JWTTokenSchemaIn(Schema):
    refresh_token: str


class TransferMoneySchemaIn(Schema):
    from_account: int
    to_account: int
    money: Decimal


class LoginSchemaIn(Schema):
    username: str
    password: str
