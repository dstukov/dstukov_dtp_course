from datetime import datetime, timedelta

from django.utils import timezone

from src.app.internal.auth.db.models import IssuedToken
from src.app.internal.auth.domain.entities import JWTTokenSchemaOut
from src.app.internal.users.db.models import User
from src.app.internal.users.domain.services import IUserRepository
from src.app.internal.utils.exceptions import (
    RefreshTokenExpiredError,
    RefreshTokenHasBeenRevokedError,
    RefreshTokenNotRecognizedError,
    UserNotFoundError,
)
from src.app.internal.utils.utils import (
    JWT_ACCESS_TOKEN_EXPIRED,
    JWT_REFRESH_TOKEN_EXPIRED,
    generate_access_token,
    generate_refresh_token,
    is_token_expired,
    try_parse_token,
)


class IIssuedTokenRepository:
    def revoke_user_tokens(self, user_id: int) -> None:
        ...

    def find_token_with_user_by_id(self, token_id: int) -> IssuedToken:
        ...

    def create_token(self, user_id: int, refresh_expires_at: datetime) -> IssuedToken:
        ...


class AuthService:
    def __init__(self, user_repo: IUserRepository, token_repo: IIssuedTokenRepository):
        self.user_repo = user_repo
        self.token_repo = token_repo

    async def login(self, username: str, password: str) -> JWTTokenSchemaOut:
        if username is None or password is None:
            raise ValueError
        user = await self.user_repo.find_by_username(username)
        if user is None or not user.check_password(password):
            raise UserNotFoundError
        self.token_repo.revoke_user_tokens(user.id)
        return self.generate_token(user)

    def refresh(self, raw_refresh_token: str | None) -> JWTTokenSchemaOut:
        success, refresh_token = try_parse_token(raw_refresh_token)
        if not success:
            raise RefreshTokenNotRecognizedError

        issued_token = self.token_repo.find_token_with_user_by_id(refresh_token["id"])
        if issued_token is None:
            raise RefreshTokenNotRecognizedError

        user = issued_token.user
        self.token_repo.revoke_user_tokens(user.id)
        if issued_token.revoked:
            raise RefreshTokenHasBeenRevokedError
        if is_token_expired(issued_token):
            raise RefreshTokenExpiredError

        return self.generate_token(user)

    def generate_token(self, user: User) -> JWTTokenSchemaOut:
        access_token = generate_access_token(user.id, timedelta(seconds=JWT_ACCESS_TOKEN_EXPIRED))
        refresh_expiry_delta = timedelta(seconds=JWT_REFRESH_TOKEN_EXPIRED)
        refresh_expires_at = timezone.now() + refresh_expiry_delta
        token = self.token_repo.create_token(user.id, refresh_expires_at)
        refresh_jwt_token = generate_refresh_token(token.id, refresh_expiry_delta)

        return JWTTokenSchemaOut(refresh_token=refresh_jwt_token, access_token=access_token)
