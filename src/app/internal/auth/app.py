from ninja import NinjaAPI

from src.app.internal.auth.db.repositories import IssuedTokenRepository
from src.app.internal.auth.domain.services import AuthService
from src.app.internal.auth.presentation.handlers import AuthHandlers
from src.app.internal.auth.presentation.routers import add_auth_router
from src.app.internal.users.db.repositories import UserRepository


def configure_auth_api(api: NinjaAPI):
    user_repo = UserRepository()
    token_repo = IssuedTokenRepository()
    auth_service = AuthService(user_repo, token_repo)
    auth_handlers = AuthHandlers(auth_service)
    add_auth_router(api, auth_handlers)
