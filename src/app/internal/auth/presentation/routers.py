from ninja import NinjaAPI, Router

from src.app.internal.auth.domain.entities import JWTTokenSchemaOut
from src.app.internal.auth.presentation.handlers import AuthHandlers


def get_auth_router(auth_handlers: AuthHandlers):
    router = Router(tags=["auth"])

    router.add_api_operation(
        "login/",
        ["POST"],
        auth_handlers.login,
        response={200: JWTTokenSchemaOut},
        auth=None,
    )
    router.add_api_operation(
        "refresh/",
        ["POST"],
        auth_handlers.refresh,
        response={200: JWTTokenSchemaOut},
        auth=None,
    )

    return router


def add_auth_router(api: NinjaAPI, auth_handlers: AuthHandlers):
    auth_router = get_auth_router(auth_handlers)
    api.add_router("auth", auth_router)
