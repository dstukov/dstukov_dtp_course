from django.contrib import admin

from src.app.internal.auth.db.models import IssuedToken


@admin.register(IssuedToken)
class IssuedTokenAdmin(admin.ModelAdmin):
    pass
