from django.http import HttpRequest, JsonResponse
from ninja import Body

from src.app.internal.auth.domain.entities import JWTTokenSchemaIn, LoginSchemaIn
from src.app.internal.auth.domain.services import AuthService
from src.app.internal.utils.exceptions import (
    RefreshTokenExpiredError,
    RefreshTokenHasBeenRevokedError,
    RefreshTokenNotRecognizedError,
    UserNotFoundError,
)


class AuthHandlers:
    def __init__(self, auth_service: AuthService):
        self.auth_service = auth_service

    async def login(self, _: HttpRequest, model: LoginSchemaIn = Body(...)):
        try:
            return await self.auth_service.login(model.username, model.password)
        except ValueError:
            return JsonResponse({"detail": "username and password must be present"}, status=400)
        except UserNotFoundError:
            return JsonResponse({"detail": "User not found"}, status=401)

    def refresh(self, _: HttpRequest, model: JWTTokenSchemaIn = Body(...)):
        try:
            return self.auth_service.refresh(model.refresh_token)
        except ValueError:
            return JsonResponse({"detail": 'key "refresh_token" must be present'}, status=400)
        except RefreshTokenNotRecognizedError:
            return JsonResponse({"detail": "Refresh token is not recognized"}, status=400)
        except RefreshTokenExpiredError:
            return JsonResponse({"detail": "Refresh token is expired"}, status=400)
        except RefreshTokenHasBeenRevokedError:
            return JsonResponse({"detail": "Refresh token has been revoked"}, status=400)
