from ninja import Schema


class ErrorResponse(Schema):
    message: str


class Unauthorized(ErrorResponse):
    ...


class Forbidden(ErrorResponse):
    ...


class Ok(Schema):
    ...
