from django.http import HttpRequest
from ninja import NinjaAPI

from src.app.internal.auth.db.models import IssuedToken
from src.app.internal.bank.db.models import PaymentAccount, PaymentCard
from src.app.internal.users.db.models import Favorite, User
from src.app.internal.utils.exceptions import FavoriteAlreadyAddedError, RequirePhoneError, UnauthorizedError


def configure_error_handlers(api: NinjaAPI):
    add_handler(api, UnauthorizedError, unauthorized_exception_handler)
    add_custom_400_exceptions(api)


def add_handler(api, exc, handler):
    def wrapper(request, e):
        handler(request, e, api)

    api.add_exception_handler(exc, wrapper)


def unauthorized_exception_handler(request: HttpRequest, e: UnauthorizedError, api: NinjaAPI):
    return api.create_response(request, {"message": f"UnauthorizedException. {e.detail or ''}"}, status=401)


def create_message_handler(message, api):
    def wrapped(request, e):
        return api.create_response(request, {"detail": message}, status=400)

    return wrapped


def add_custom_400_exceptions(api: NinjaAPI):
    mp = map_exception_to_message()
    for exception_type, message in mp.items():
        api.add_exception_handler(exception_type, create_message_handler(message, api))


def map_exception_to_message():
    return {
        FavoriteAlreadyAddedError: "favorite already added",
        RequirePhoneError: "need phone",
        User.DoesNotExist: "user not exist",
        Favorite.DoesNotExist: "favorite not exist",
        IssuedToken.DoesNotExist: "token not exist",
        PaymentCard.DoesNotExist: "card not exist",
        PaymentAccount.DoesNotExist: "account not exist",
    }
