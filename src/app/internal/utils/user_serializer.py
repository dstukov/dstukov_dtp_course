from typing import Any, Callable

from src.app.internal.users.db.models import User


# it was possible to use ready-made serializer, but I decide to adapt it for telegram too
def serialize_user(user: User, field_wrapper: Callable[[Any], Any] = lambda x: x):
    return f"""
Id: {field_wrapper(user.id)}
Chat Id: {field_wrapper(user.chat_id)}
Username: {field_wrapper(user.username)}
First name: {field_wrapper(user.first_name)}
Last name: {field_wrapper(user.last_name)}
Phone number: {field_wrapper(user.phone_number)}
Is superuser: {field_wrapper(user.is_superuser)}
Is blocked bot: {field_wrapper(user.is_blocked_bot)}
Is banned: {field_wrapper(user.is_banned)}
"""


def serialize_user_telegram(user: User):
    return serialize_user(user, lambda x: f"<pre>{x}</pre>")
