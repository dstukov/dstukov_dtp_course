import calendar
import datetime
import re

import jwt
from django.utils import timezone

from src.config import env

LUHN_ODD_LOOKUP = (0, 2, 4, 6, 8, 1, 3, 5, 7, 9)
re_non_digits = re.compile(r"\D+")
JWT_ALGORITHM = "HS256"
JWT_SECRET_KEY = env("JWT_SECRET")
JWT_ACCESS_TOKEN_EXPIRED = env("JWT_ACCESS_TOKEN_EXPIRED", cast=int, default=600)
JWT_REFRESH_TOKEN_EXPIRED = env("JWT_REFRESH_TOKEN_EXPIRED", cast=int, default=86400)


def get_digits(value: str) -> str:
    """
    Get all digits from input string.
    """
    if not value:
        return ""
    return re_non_digits.sub("", str(value))


def luhn(number: str) -> bool:
    """
    Validate credit card number with Luhn's Algorithm.
    Source:
    https://github.com/django/django-localflavor/blob/master/localflavor/generic/checksums.py
    """
    try:
        evens = sum(int(c) for c in number[-1::-2])
        odds = sum(LUHN_ODD_LOOKUP[int(c)] for c in number[-2::-2])
        return (evens + odds) % 10 == 0
    except ValueError:
        return False


def expiry_date(year: int, month: int) -> datetime.date:
    """
    Return the last day of month.
    """
    weekday, day_count = calendar.monthrange(year, month)
    return datetime.date(year, month, day_count)


def try_parse_token(raw_token: str | None):
    if raw_token is None:
        return False, None
    try:
        return True, jwt.decode(raw_token, JWT_SECRET_KEY, algorithms=[JWT_ALGORITHM], options=dict(verify_exp=False))
    except Exception:
        return False, None


def generate_access_token(user_id, expired_time):
    time = timezone.now() + expired_time
    token = jwt.encode(
        payload={"id": user_id, "exp": int(time.timestamp())}, key=JWT_SECRET_KEY, algorithm=JWT_ALGORITHM
    )
    return token


def generate_refresh_token(token_id, expired_time):
    dt = (timezone.now() + expired_time).timestamp()
    return jwt.encode(payload={"id": token_id, "exp": int(dt)}, key=JWT_SECRET_KEY, algorithm=JWT_ALGORITHM)


def is_token_expired(token):
    expired_date = token.expires_at
    if expired_date is None:
        return True
    return expired_date < timezone.now()
