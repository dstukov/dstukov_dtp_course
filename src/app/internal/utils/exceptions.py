from typing import Optional


class UserNotFoundError(Exception):
    ...


class RequirePhoneError(Exception):
    ...


class RefreshTokenHasBeenRevokedError(Exception):
    ...


class RefreshTokenExpiredError(Exception):
    ...


class RefreshTokenNotRecognizedError(Exception):
    ...


class IncorrectUserPasswordError(Exception):
    ...


class UnauthorizedError(Exception):
    def __init__(self, detail: Optional[str] = None):
        self.detail = detail


class FavoriteAlreadyAddedError(Exception):
    ...
