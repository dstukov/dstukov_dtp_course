from django.urls import path

from src.app.internal.handlers.rest.handlers import api

urlpatterns = [path("me", api.urls)]
