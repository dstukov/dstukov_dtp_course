from typing import Dict, Final, TypedDict

PHONE_REQUIRED_FLAG = "phone_required"


class BotCommandMeta(TypedDict):
    description: str
    flags: dict


BOT_COMMANDS: Final[Dict[str, BotCommandMeta]] = {
    "start": BotCommandMeta(description="Register in bot", flags={}),
    "set_phone": BotCommandMeta(description="Set phone number for user", flags={}),
    "me": BotCommandMeta(description="Get information about user", flags={PHONE_REQUIRED_FLAG: True}),
    "payment_account_balance": BotCommandMeta(
        description="Get payment account balance", flags={PHONE_REQUIRED_FLAG: True}
    ),
    "set_favorite": BotCommandMeta(
        description="Set favorite user by username or id", flags={PHONE_REQUIRED_FLAG: True}
    ),
    "delete_favorite": BotCommandMeta(
        description="Delete favorite user by username or id", flags={PHONE_REQUIRED_FLAG: True}
    ),
    "favorites": BotCommandMeta(description="Show favorite users", flags={PHONE_REQUIRED_FLAG: True}),
    "transfer_money": BotCommandMeta(
        description="Transfer money to user in format {money} {username or account_id}",
        flags={PHONE_REQUIRED_FLAG: True},
    ),
    "get_transaction_users": BotCommandMeta(
        description="Get users you have interacted with", flags={PHONE_REQUIRED_FLAG: True}
    ),
    "get_account_statement": BotCommandMeta(
        description="Get your account statement", flags={PHONE_REQUIRED_FLAG: True}
    ),
    "new_transactions": BotCommandMeta(description="Get not seen transactions", flags={PHONE_REQUIRED_FLAG: True}),
}
