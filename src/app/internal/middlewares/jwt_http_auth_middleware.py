from datetime import datetime

from django.http import HttpRequest
from ninja.security import HttpBearer

from src.app.internal.users.db.models import User
from src.app.internal.utils.utils import try_parse_token


class JWTHttpAuthMiddleware(HttpBearer):
    def authenticate(self, request: HttpRequest, token: str):
        try:
            success, payload = try_parse_token(token)
            if not success:
                return None

            expired_date = datetime.fromtimestamp(payload["exp"])
            if expired_date < datetime.now():
                return None

            request.user = User.objects.get(id=payload["id"])
        except Exception:
            return None
        return token
