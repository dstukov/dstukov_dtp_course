from typing import Any, Awaitable, Callable, Dict

from aiogram import BaseMiddleware
from aiogram.dispatcher.flags import get_flag
from aiogram.types import Message

from src.app.internal.users.db.repositories import UserRepository
from src.app.internal.users.domain.services import UserService
from src.app.internal.utils.constants import PHONE_REQUIRED_FLAG

user_repo = UserRepository()
user_service = UserService(user_repo)


class PhoneRequiredMiddleware(BaseMiddleware):
    async def __call__(
        self, handler: Callable[[Message, Dict[str, Any]], Awaitable[Any]], event: Message, data: Dict[str, Any]
    ) -> Any:
        phone_required_flag = get_flag(data, PHONE_REQUIRED_FLAG)

        if not phone_required_flag:
            return await handler(event, data)

        user = await user_repo.get_by_chat_id(event.from_user.id)

        if user.phone_number is not None and len(user.phone_number) > 0:
            return await handler(event, data)
        else:
            return await event.reply("Set phone number to use that method")
