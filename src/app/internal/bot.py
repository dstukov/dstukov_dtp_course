import logging
from typing import TypedDict

from aiogram import Bot, Dispatcher
from aiogram.types import BotCommand
from aiogram.webhook.aiohttp_server import SimpleRequestHandler, setup_application
from aiohttp.web import run_app
from aiohttp.web_app import Application

from src.app.internal.handlers.bot.handlers import router
from src.app.internal.utils.constants import BOT_COMMANDS, BotCommandMeta
from src.config import env
from src.config.bot import TG_TOKEN


def _map_bot_command(items: (str, BotCommandMeta)) -> BotCommand:
    return BotCommand(command=f"/{items[0]}", description=items[1]["description"])


def _set_bot_commands(bot) -> None:
    bot_commands = list(map(_map_bot_command, BOT_COMMANDS.items()))
    bot.set_my_commands(bot_commands)


HOST = env("HOST")
WEBHOOK_PATH = env("WEBHOOK_PATH", default="/webhook")
WEBHOOK_URL = f"{HOST}{WEBHOOK_PATH}"

WEBAPP_HOST = env("WEBAPP_HOST")
PORT = env("PORT")


@router.startup()
async def on_startup(bot: Bot, webhook_url: str):
    bot_commands = list(map(_map_bot_command, BOT_COMMANDS.items()))
    await bot.set_my_commands(bot_commands)
    await bot.set_webhook(webhook_url)


@router.shutdown()
async def on_shutdown(bot: Bot):
    await bot.delete_webhook()


def main():
    bot = Bot(TG_TOKEN, parse_mode="HTML")
    dispatcher = Dispatcher()
    dispatcher["webhook_url"] = WEBHOOK_URL
    dispatcher.include_router(router)

    app = Application()

    SimpleRequestHandler(
        dispatcher=dispatcher,
        bot=bot,
    ).register(app, path=WEBHOOK_PATH)
    setup_application(app, dispatcher, bot=bot)

    run_app(app, host=WEBAPP_HOST, port=PORT)


if __name__ == "__main__":
    main()
    logging.basicConfig(level=logging.INFO)
