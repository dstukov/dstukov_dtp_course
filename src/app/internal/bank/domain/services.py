from datetime import datetime
from typing import List

from _decimal import Decimal
from asgiref.sync import sync_to_async
from django.db.models import QuerySet

from src.app.internal.bank.db.models import PaymentAccount, PaymentCard, Transaction
from src.app.internal.bank.domain.entities import AccountStatementDto, LastTransactionDto
from src.app.internal.postcards.db.models import Postcard
from src.app.internal.users.db.models import User


class ITransactionRepository:
    async def get_by_id(self, transaction_id: int) -> Transaction:
        ...

    async def create(self, sender_account_id: int, recipient_account_id: int, money: Decimal) -> Transaction:
        ...

    @sync_to_async
    def get_recipients_usernames(self, chat_id: int) -> QuerySet[str]:
        ...

    @sync_to_async
    def get_senders_usernames(self, chat_id: int) -> QuerySet[str]:
        ...

    @sync_to_async
    def get_last_transactions(self, bank_account_id: int, from_date: datetime) -> List[LastTransactionDto]:
        ...

    @sync_to_async
    def get_not_viewed_transactions(self, bank_account_id: int) -> List[Transaction]:
        ...

    @sync_to_async
    def attach_postcard(self, trans: Transaction, postcard: Postcard) -> None:
        ...

    @sync_to_async
    def mark_transaction_viewed(self, trans: Transaction) -> None:
        ...


class TransactionService:
    def __init__(self, transaction_repo: ITransactionRepository):
        self.transaction_repo = transaction_repo

    async def get_by_id(self, transaction_id: int) -> Transaction:
        return await self.transaction_repo.get_by_id(transaction_id)

    async def create(
        self, sender_account: PaymentAccount, recipient_account: PaymentAccount, money: Decimal
    ) -> Transaction:
        return await self.transaction_repo.create(sender_account.id, recipient_account.id, money)

    async def get_recipients_usernames(self, chat_id: int) -> QuerySet[str]:
        return await self.transaction_repo.get_recipients_usernames(chat_id)

    async def get_senders_usernames(self, chat_id: int) -> QuerySet[str]:
        return await self.transaction_repo.get_senders_usernames(chat_id)

    async def get_transaction_history(self, chat_id: int) -> List[str]:
        recipients = await self.get_recipients_usernames(chat_id)
        senders = await self.get_senders_usernames(chat_id)
        return list(recipients.union(senders))

    async def get_last_transactions(self, bank_account_id: int, from_date: datetime) -> List[LastTransactionDto]:
        return await self.transaction_repo.get_last_transactions(bank_account_id, from_date)

    async def get_not_viewed_transactions(self, bank_account_id: int) -> List[Transaction]:
        return await self.transaction_repo.get_not_viewed_transactions(bank_account_id)

    async def attach_postcard(self, trans: Transaction, postcard: Postcard) -> None:
        return await self.transaction_repo.attach_postcard(trans, postcard)

    async def mark_transaction_viewed(self, trans: Transaction) -> None:
        return await self.transaction_repo.mark_transaction_viewed(trans)


class IPaymentAccountRepository:
    async def get_by_id(self, account_id: int) -> PaymentAccount:
        ...

    async def get_by_chat_id(self, chat_id: int) -> PaymentAccount:
        ...

    async def get_money_by_chat_id(self, chat_id: int) -> Decimal:
        ...

    async def create(self, account_id: int, user: User, money: Decimal) -> PaymentAccount:
        ...

    async def get_payment_account_cards(self, chat_id: int) -> List[str]:
        ...

    async def transfer_money(
        self, sender_account: PaymentAccount, recipient_account: PaymentAccount, money: Decimal
    ) -> Transaction:
        ...


class PaymentAccountService:
    def __init__(self, account_repo: IPaymentAccountRepository, transaction_repo: ITransactionRepository):
        self.account_repo = account_repo
        self.transaction_repo = transaction_repo

    async def get_by_id(self, account_id: int) -> PaymentAccount:
        return await self.account_repo.get_by_id(account_id)

    async def get_by_chat_id(self, chat_id: int) -> PaymentAccount:
        return await self.account_repo.get_by_chat_id(chat_id)

    async def get_money_by_chat_id(self, chat_id: int) -> Decimal:
        return await self.account_repo.get_money_by_chat_id(chat_id)

    async def create(self, account_id: int, user: User, money: Decimal) -> PaymentAccount:
        return await self.account_repo.create(account_id, user, money)

    async def transfer_money(
        self, sender_account: PaymentAccount, recipient_account: PaymentAccount, money: Decimal
    ) -> Transaction:
        return await self.account_repo.transfer_money(sender_account, recipient_account, money)

    async def get_payment_account_cards(self, chat_id: int) -> List[str]:
        return await self.account_repo.get_payment_account_cards(chat_id)


class IPaymentCardRepository:
    async def get_by_id(self, card_id: str) -> PaymentCard:
        ...

    async def create(self, payment_account: PaymentAccount, card_number: str) -> PaymentCard:
        ...

    @sync_to_async
    def get_card_numbers_by_payment_account_id(self, payment_account_id: int) -> [PaymentCard]:
        ...


class PaymentCardService:
    def __init__(self, payment_card_repo: IPaymentCardRepository):
        self.payment_card_repo = payment_card_repo

    async def get_by_id(self, card_id: str) -> PaymentCard:
        return await self.payment_card_repo.get_by_id(card_id)

    async def create(self, payment_account: PaymentAccount, card_number: str) -> PaymentCard:
        return await self.payment_card_repo.create(payment_account, card_number)

    async def get_card_numbers_by_payment_account_id(self, payment_account_id: int) -> [PaymentCard]:
        return await self.payment_card_repo.get_card_numbers_by_payment_account_id(payment_account_id)


class AccountStatementService:
    def __init__(
        self,
        account_repo: IPaymentAccountRepository,
        card_repo: IPaymentCardRepository,
        transaction_repo: ITransactionRepository,
    ):
        self.account_repo = account_repo
        self.card_repo = card_repo
        self.transaction_repo = transaction_repo

    async def get_account_statement(self, chat_id: int, from_date: datetime) -> AccountStatementDto:
        payment_account = await self.account_repo.get_by_chat_id(chat_id)
        card_numbers = await self.card_repo.get_card_numbers_by_payment_account_id(payment_account.id)
        latest_transactions = await self.transaction_repo.get_last_transactions(payment_account.id, from_date)

        return AccountStatementDto(payment_account, card_numbers, latest_transactions)
