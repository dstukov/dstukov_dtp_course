from datetime import datetime
from typing import List

from src.app.internal.bank.db.models import PaymentAccount
from src.app.internal.postcards.db.repositories import PostcardRepository


class LastTransactionDto:
    sender_username: str
    recipient_username: str
    money: float
    date: datetime
    postcard_id: int | None

    def __init__(
        self,
        sender_username: str,
        recipient_username: str,
        money: float,
        date: datetime,
        postcard_id: int | None,
    ):
        self.sender_username = sender_username
        self.recipient_username = recipient_username
        self.money = money
        self.date = date
        self.postcard_id = postcard_id

    def __str__(self):
        result = f"Sender: {self.sender_username}, Recipient: {self.recipient_username}, money: {self.money}, date: {self.date}"
        if self.postcard_id is not None:
            postcard_repository = PostcardRepository()
            presigned_url = postcard_repository.get_postcard_by_id(self.postcard_id).image.url
            result += f", <a href='{presigned_url}'>postcard</a>"
        return result


class AccountStatementDto:
    payment_account: PaymentAccount
    card_numbers: List[str]
    latest_transactions: List[LastTransactionDto]

    def __init__(
        self, payment_account: PaymentAccount, card_numbers: List[str], latest_transactions: List[LastTransactionDto]
    ):
        self.payment_account = payment_account
        self.card_numbers = card_numbers
        self.latest_transactions = latest_transactions

    def __str__(self):
        transactions = "\n".join(map(lambda x: str(x), self.latest_transactions))
        return f"""Payment account id: {self.payment_account.id}
Money: {self.payment_account.money}
Card numbers: {", ".join(self.card_numbers)}
Latest transactions:\n{transactions}
"""
