from django.core.exceptions import ValidationError
from django.db import models
from django.utils import timezone

from src.app.internal import utils
from src.app.internal.postcards.db.models import Postcard
from src.app.internal.users.db.models import User


class PaymentAccount(models.Model):
    id = models.BigAutoField(primary_key=True, auto_created=True, serialize=False)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    money = models.DecimalField(default=0, max_digits=10, decimal_places=2, verbose_name="money")

    objects: models.manager.BaseManager["PaymentAccount"]


def payment_card_validator(value):
    if not utils.luhn(utils.get_digits(value)):
        raise ValidationError("Enter a valid credit card number.")


class PaymentCard(models.Model):
    id = models.BigAutoField(primary_key=True, auto_created=True, serialize=False)
    payment_account_id = models.ForeignKey(PaymentAccount, on_delete=models.CASCADE)
    card_number = models.CharField(max_length=20, validators=[payment_card_validator])

    objects = models.manager.BaseManager["PaymentCard"]


class Transaction(models.Model):
    id = models.BigAutoField(auto_created=True, primary_key=True)
    sender_account = models.ForeignKey(PaymentAccount, on_delete=models.CASCADE, related_name="sender_account")
    recipient_account = models.ForeignKey(PaymentAccount, on_delete=models.CASCADE, related_name="recipient_account")
    money = models.DecimalField(max_digits=10, decimal_places=2)
    date = models.DateTimeField(default=timezone.now)
    postcard = models.ForeignKey(Postcard, on_delete=models.SET_NULL, null=True)
    viewed = models.BooleanField(default=False)

    class Meta:
        verbose_name = "Transaction"
        verbose_name_plural = "Transactions"
