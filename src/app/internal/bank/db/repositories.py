from datetime import datetime
from typing import List

from _decimal import Decimal
from asgiref.sync import sync_to_async
from django.db import transaction
from django.db.models import F, Q, QuerySet

from src.app.internal.bank.db.models import PaymentAccount, PaymentCard, Transaction
from src.app.internal.bank.domain.entities import LastTransactionDto
from src.app.internal.bank.domain.services import (
    IPaymentAccountRepository,
    IPaymentCardRepository,
    ITransactionRepository,
)
from src.app.internal.postcards.db.models import Postcard
from src.app.internal.users.db.models import User
from src.app.internal.users.domain.services import IUserRepository


class TransactionRepository(ITransactionRepository):
    async def get_by_id(self, transaction_id: int) -> Transaction:
        return await Transaction.objects.aget(id=transaction_id)

    async def create(self, sender_account_id: int, recipient_account_id: int, money: Decimal) -> Transaction:
        return await Transaction.objects.acreate(
            sender_account_id=sender_account_id, recipient_account_id=recipient_account_id, money=money
        )

    @sync_to_async
    def get_recipients_usernames(self, chat_id: int) -> QuerySet[str]:
        return Transaction.objects.filter(sender_account__user__chat_id=chat_id).values_list(
            "recipient_account__user__username", flat=True
        )

    @sync_to_async
    def get_senders_usernames(self, chat_id: int) -> QuerySet[str]:
        return Transaction.objects.filter(recipient_account__user__chat_id=chat_id).values_list(
            "sender_account__user__username", flat=True
        )

    @sync_to_async
    def get_last_transactions(self, bank_account_id: int, from_date: datetime) -> List[LastTransactionDto]:
        result = (
            Transaction.objects.filter(Q(date__gte=from_date))
            .filter(Q(sender_account__id=bank_account_id) | Q(recipient_account__id=bank_account_id))
            .order_by("-date")
            .values_list(
                "sender_account__user__username",
                "recipient_account__user__username",
                "money",
                "date",
                "postcard__id",
            )
        )

        return [LastTransactionDto(*values) for values in result]

    @sync_to_async
    def attach_postcard(self, trans: Transaction, postcard: Postcard) -> None:
        trans.postcard = postcard
        trans.save()

    @sync_to_async
    def mark_transaction_viewed(self, trans: Transaction) -> None:
        trans.viewed = True
        trans.save()

    @sync_to_async
    def get_not_viewed_transactions(self, bank_account_id: int) -> List[Transaction]:
        return Transaction.objects.filter(
            Q(sender_account__id=bank_account_id) | Q(recipient_account__id=bank_account_id), viewed=False
        )


class PaymentAccountRepository(IPaymentAccountRepository):
    def __init__(self, transaction_repo: ITransactionRepository, user_repo: IUserRepository):
        self.transaction_repo = transaction_repo
        self.user_repo = user_repo

    async def get_by_id(self, account_id: str) -> PaymentAccount:
        return await PaymentAccount.objects.aget(id=account_id)

    async def get_by_chat_id(self, chat_id: int) -> PaymentAccount:
        return await PaymentAccount.objects.aget(user__chat_id=chat_id)

    async def get_money_by_chat_id(self, chat_id: int) -> Decimal:
        return await PaymentAccount.objects.filter(user__chat_id=chat_id).values_list("money", flat=True).afirst()

    async def create(self, account_id: str, user: User, money: Decimal) -> PaymentAccount:
        return await PaymentAccount.objects.acreate(id=account_id, user=user, money=money)

    async def get_payment_account_cards(self, chat_id: int) -> List[str]:
        user = await self.user_repo.get_by_chat_id(chat_id)
        return list(PaymentCard.objects.filter(payment_account_id__user=user.id).values_list("card_number", flat=True))

    async def transfer_money(
        self, sender_account: PaymentAccount, recipient_account: PaymentAccount, money: Decimal
    ) -> Transaction:
        with transaction.atomic():
            sender_account.money = F("money") - money
            recipient_account.money = F("money") + money
            PaymentAccount.objects.bulk_update([sender_account, recipient_account], ["money"], 2)
            return await self.transaction_repo.create(sender_account.id, recipient_account.id, money)


class PaymentCardRepository(IPaymentCardRepository):
    async def get_by_id(self, card_id: str) -> PaymentCard:
        return await PaymentCard.objects.aget(id=card_id)

    async def create(self, payment_account: PaymentAccount, card_number: str) -> PaymentCard:
        return await PaymentCard.objects.acreate(payment_account_id=payment_account, card_number=card_number)

    @sync_to_async
    def get_card_numbers_by_payment_account_id(self, payment_account_id: int) -> [PaymentCard]:
        return PaymentCard.objects.filter(payment_account_id__id=payment_account_id).values_list(
            "card_number", flat=True
        )
