from ninja import NinjaAPI

from src.app.internal.bank.db.repositories import PaymentAccountRepository, TransactionRepository
from src.app.internal.bank.domain.services import PaymentAccountService
from src.app.internal.bank.presentation.handlers import MoneyHandlers
from src.app.internal.bank.presentation.routers import add_bank_routers
from src.app.internal.users.db.repositories import UserRepository


def configure_bank_api(api: NinjaAPI):
    user_repo = UserRepository()
    transaction_repo = TransactionRepository()
    account_repo = PaymentAccountRepository(transaction_repo, user_repo)
    account_service = PaymentAccountService(account_repo, transaction_repo)
    money_handlers = MoneyHandlers(account_service)
    add_bank_routers(api, money_handlers)
