from django.contrib import admin

from src.app.internal.bank.db.models import PaymentAccount, PaymentCard, Transaction


@admin.register(PaymentAccount)
class PaymentAccountAdmin(admin.ModelAdmin):
    pass


@admin.register(PaymentCard)
class PaymentCardAdmin(admin.ModelAdmin):
    pass


@admin.register(Transaction)
class TransactionAdmin(admin.ModelAdmin):
    pass
