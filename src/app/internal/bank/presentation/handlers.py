from django.http import HttpRequest
from ninja import Body

from src.app.internal.auth.domain.entities import TransferMoneySchemaIn
from src.app.internal.bank.domain.services import PaymentAccountService
from src.app.internal.utils.exceptions import UnauthorizedError
from src.app.internal.utils.http_responses import Ok


class MoneyHandlers:
    def __init__(self, account_service: PaymentAccountService):
        self.account_service = account_service

    async def transfer_money(self, request: HttpRequest, data: TransferMoneySchemaIn = Body(...)) -> Ok:
        user = request.user
        if user is None:
            raise UnauthorizedError()
        from_account = await self.account_service.get_by_id(data.from_account)
        to_account = await self.account_service.get_by_id(data.to_account)
        await self.account_service.transfer_money(from_account, to_account, data.money)
        return Ok()
