from ninja import NinjaAPI, Router

from src.app.internal.bank.presentation.handlers import MoneyHandlers
from src.app.internal.middlewares.jwt_http_auth_middleware import JWTHttpAuthMiddleware
from src.app.internal.utils.http_responses import Forbidden, Ok, Unauthorized


def get_money_router(money_handlers: MoneyHandlers):
    router = Router(tags=["money"])

    router.add_api_operation(
        "transfer/",
        ["POST"],
        money_handlers.transfer_money,
        response={200: Ok, 401: Unauthorized, 403: Forbidden},
        auth=[JWTHttpAuthMiddleware()],
    )

    return router


def add_bank_routers(api: NinjaAPI, money_handlers: MoneyHandlers):
    api.add_router("money", get_money_router(money_handlers))
