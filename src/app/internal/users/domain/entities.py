from typing import List

from ninja import Schema
from ninja.orm import create_schema

from src.app.internal.users.db.models import User

UserSchema = create_schema(User, fields=["id", "chat_id", "username", "first_name", "last_name", "phone_number"])


class UserSchemaOut(UserSchema):
    ...


class FavoriteUsernameSchemaIn(Schema):
    username: str


class FavoritesSchemaOut(Schema):
    favorites: List[str] = []
