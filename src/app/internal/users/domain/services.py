from typing import List

from src.app.internal.users.db.models import Favorite, User
from src.app.internal.users.domain.entities import UserSchemaOut
from src.app.internal.utils.exceptions import FavoriteAlreadyAddedError, RequirePhoneError


class IUserRepository:
    async def register_bot_user(
        self,
        chat_id: int,
        username: str | None,
        first_name: str,
        last_name: str | None,
    ) -> tuple[User, bool]:
        ...

    async def get_by_id(self, user_id: int) -> User:
        ...

    async def get_by_chat_id(self, chat_id: int) -> User:
        ...

    async def get_by_username(self, username: str) -> User:
        ...

    async def find_by_username(self, username: str) -> User:
        ...

    async def set_phone(self, chat_id: int, phone_number: str) -> User:
        ...

    async def update_password(self, chat_id: int, password: str, previous_password: str):
        ...


class UserService:
    def __init__(self, user_repo: IUserRepository):
        self.user_repo = user_repo

    async def register_bot_user(
        self,
        chat_id: int,
        username: str | None,
        first_name: str,
        last_name: str | None,
    ) -> tuple[User, bool]:
        return await self.user_repo.register_bot_user(chat_id, username, first_name, last_name)

    async def get_by_id(self, user_id: int) -> User:
        return await self.user_repo.get_by_id(user_id)

    async def get_by_chat_id(self, chat_id: int) -> User:
        return await self.user_repo.get_by_chat_id(chat_id)

    async def get_by_username(self, username: str | None) -> User:
        if username is None:
            raise Exception("empty username")

        return await self.user_repo.get_by_username(username)

    async def find_by_username(self, username: str) -> User:
        return await self.user_repo.find_by_username(username)

    async def get_user_schema(self, chat_id: int) -> UserSchemaOut:
        user = await self.user_repo.get_by_id(chat_id)
        if not user.phone_number:
            raise RequirePhoneError
        return UserSchemaOut.from_orm(user)

    async def set_phone(self, chat_id: int, phone_number: str) -> User:
        return await self.user_repo.set_phone(chat_id, phone_number)

    async def update_password(self, chat_id: int, password: str, previous_password: str):
        return await self.user_repo.update_password(chat_id, password, previous_password)


class IFavoriteRepository:
    async def add_favorite_for(self, user_id: int, favorite_user_id: int) -> Favorite:
        ...

    async def get_favorite_for(self, chat_id: int, favorite_chat_id: int) -> Favorite:
        ...

    async def remove_favorite_by_id(self, chat_id: int, favorite_chat_id: int) -> bool:
        ...

    async def get_favorites(self, chat_id: int) -> [Favorite]:
        ...


class FavoriteService:
    def __init__(self, user_repo: IUserRepository, favorite_repo: IFavoriteRepository):
        self.user_repo = user_repo
        self.favorite_repo = favorite_repo

    async def create_favorite(self, user_chat_id: int, favorite_user_chat_id: int) -> Favorite:
        user = await self.user_repo.get_by_chat_id(user_chat_id)
        favorite = await self.user_repo.get_by_chat_id(favorite_user_chat_id)
        return await self.favorite_repo.add_favorite_for(user.id, favorite.id)

    async def get_favorite_for(self, chat_id: int, favorite_chat_id: int) -> Favorite:
        return await self.favorite_repo.get_favorite_for(chat_id, favorite_chat_id)

    async def is_favorite_for(self, user_id: int, favorite_user_id: int) -> bool:
        try:
            await self.favorite_repo.get_favorite_for(user_id, favorite_user_id)
            return True
        except Favorite.DoesNotExist:
            return False

    async def add_favorite_by_chat_id(self, user_id: int, favorite_user_id: int) -> Favorite:
        if favorite_user_id == user_id:
            # raise custom classes instead of exceptions with manual strings
            raise Exception("should add favorite only to another user")

        if await self.is_favorite_for(user_id, favorite_user_id):
            user = await self.favorite_repo.get_favorite_for(user_id, favorite_user_id)
            raise FavoriteAlreadyAddedError(f"user is already in favorite's list. {user}")

        try:
            await self.user_repo.get_by_chat_id(user_id)
        except User.DoesNotExist:
            raise Exception("user not found")

        return await self.favorite_repo.add_favorite_for(user_id, favorite_user_id)

    async def add_favorite_by_username(self, user_id: int, favorite_username: str | None) -> Favorite:
        favorite_user = await self.user_repo.get_by_username(favorite_username)

        return await self.add_favorite_by_chat_id(user_id, favorite_user.chat_id)

    async def remove_favorite_by_id(self, chat_id: int, favorite_chat_id: int) -> bool:
        return await self.favorite_repo.remove_favorite_by_id(chat_id, favorite_chat_id)

    async def remove_favorite_by_username(self, user_id: int, favorite_username: str | None) -> None:
        favorite_user = await self.user_repo.get_by_username(favorite_username)

        deleted = await self.favorite_repo.remove_favorite_by_id(user_id, favorite_user.chat_id)
        if deleted is False:
            raise Exception("user was not in favorite's list")

    async def get_favorites(self, chat_id: int) -> List[str]:
        return await self.favorite_repo.get_favorites(chat_id)
