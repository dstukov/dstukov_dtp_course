from django.contrib import admin

from src.app.internal.users.db.models import AdminUser, Favorite, User


@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    pass


@admin.register(Favorite)
class FavoriteAdmin(admin.ModelAdmin):
    pass


@admin.register(AdminUser)
class AdminUserAdmin(admin.ModelAdmin[AdminUser]):
    pass
