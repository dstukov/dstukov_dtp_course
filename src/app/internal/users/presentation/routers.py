from ninja import NinjaAPI, Router

from src.app.internal.middlewares.jwt_http_auth_middleware import JWTHttpAuthMiddleware
from src.app.internal.users.domain.entities import FavoritesSchemaOut, UserSchema
from src.app.internal.users.presentation.handlers import FavoriteHandlers, UserHandlers
from src.app.internal.utils.http_responses import Forbidden, Ok, Unauthorized


def get_users_router(users_handlers: UserHandlers, favorite_handlers: FavoriteHandlers):
    router = Router(tags=["users"])

    router.add_api_operation(
        "me/",
        ["GET"],
        users_handlers.me,
        response={200: UserSchema, 401: Unauthorized, 403: Forbidden},
        auth=[JWTHttpAuthMiddleware()],
    )

    router.add_api_operation(
        "favorites/",
        ["GET"],
        favorite_handlers.get,
        response={200: FavoritesSchemaOut, 401: Unauthorized, 403: Forbidden},
        auth=[JWTHttpAuthMiddleware()],
    )

    router.add_api_operation(
        "favorites/",
        ["PUT"],
        favorite_handlers.put,
        response={200: Ok, 401: Unauthorized, 403: Forbidden},
        auth=[JWTHttpAuthMiddleware()],
    )

    router.add_api_operation(
        "favorites/",
        ["DELETE"],
        favorite_handlers.delete,
        response={200: FavoritesSchemaOut, 401: Unauthorized, 403: Forbidden},
        auth=[JWTHttpAuthMiddleware()],
    )

    return router


def add_users_router(api: NinjaAPI, users_handlers: UserHandlers, favorite_handlers: FavoriteHandlers):
    users_router = get_users_router(users_handlers, favorite_handlers)
    api.add_router("users", users_router)
