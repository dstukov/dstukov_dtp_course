from django.http import HttpRequest
from ninja import Body

from src.app.internal.users.domain.entities import FavoritesSchemaOut, FavoriteUsernameSchemaIn, UserSchema
from src.app.internal.users.domain.services import FavoriteService, UserService
from src.app.internal.utils.exceptions import UnauthorizedError
from src.app.internal.utils.http_responses import Ok


class UserHandlers:
    def __init__(self, user_service: UserService):
        self.user_service = user_service

    async def me(self, request: HttpRequest) -> UserSchema:
        user = request.user
        if user is None:
            raise UnauthorizedError()
        return await self.user_service.get_user_schema(user.id)


class FavoriteHandlers:
    def __init__(self, favorite_service: FavoriteService):
        self.favorite_service = favorite_service

    async def put(self, request: HttpRequest, data: FavoriteUsernameSchemaIn = Body(...)) -> Ok:
        user = request.user
        if user is None:
            raise UnauthorizedError()
        await self.favorite_service.add_favorite_by_username(user.chat_id, data.username)
        return Ok()

    async def delete(self, request: HttpRequest, data: FavoriteUsernameSchemaIn = Body(...)) -> Ok:
        user = request.user
        if user is None:
            raise UnauthorizedError()
        await self.favorite_service.remove_favorite_by_username(user.chat_id, data.username)
        return Ok()

    async def get(self, request: HttpRequest) -> FavoritesSchemaOut:
        user = request.user
        if user is None:
            raise UnauthorizedError()
        schema = FavoritesSchemaOut()
        schema.favorites = await self.favorite_service.get_favorites(user.chat_id)
        return schema
