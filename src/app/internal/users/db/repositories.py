from src.app.internal.users.db.models import Favorite, User
from src.app.internal.users.domain.services import IFavoriteRepository, IUserRepository
from src.app.internal.utils.exceptions import IncorrectUserPasswordError, RequirePhoneError


class UserRepository(IUserRepository):
    async def register_bot_user(
        self,
        chat_id: int,
        username: str | None,
        first_name: str,
        last_name: str | None,
    ) -> tuple[User, bool]:
        return await User.objects.aget_or_create(
            chat_id=chat_id, username=username, first_name=first_name, last_name=last_name
        )

    async def get_by_id(self, user_id: int) -> User:
        return await User.objects.aget(id=user_id)

    async def get_by_chat_id(self, chat_id: int) -> User:
        return await User.objects.aget(chat_id=chat_id)

    async def get_by_username(self, username: str) -> User:
        return await User.objects.aget(username=username)

    async def find_by_username(self, username: str) -> User:
        return await User.objects.filter(username=username).afirst()

    async def set_phone(self, chat_id: int, phone_number: str) -> User:
        user = await User.objects.aget(chat_id=chat_id)
        user.phone_number = phone_number
        user.save()
        return user

    async def update_password(self, chat_id: int, password: str, previous_password: str):
        user = await self.get_by_chat_id(chat_id)

        if not user.check_password(previous_password):
            raise IncorrectUserPasswordError

        user.set_password(password)
        user.save()


class FavoriteRepository(IFavoriteRepository):
    def __init__(self, user_repo: IUserRepository):
        self.users_repo = user_repo

    async def add_favorite_for(self, user_id: int, favorite_user_id: int) -> Favorite:
        user = await self.users_repo.get_by_chat_id(user_id)
        favorite = await self.users_repo.get_by_chat_id(favorite_user_id)

        return await Favorite.objects.acreate(user_id=user.id, favorite_user_id=favorite.id)

    async def get_favorite_for(self, chat_id: int, favorite_chat_id: int) -> Favorite:
        return await Favorite.objects.aget(user__chat_id=chat_id, favorite_user__chat_id=favorite_chat_id)

    async def remove_favorite_by_id(self, chat_id: int, favorite_chat_id: int) -> bool:
        deleted, _ = await Favorite.objects.filter(
            user__chat_id=chat_id, favorite_user__chat_id=favorite_chat_id
        ).adelete()
        return deleted

    async def get_favorites(self, chat_id: int) -> [Favorite]:
        user = await self.users_repo.get_by_chat_id(chat_id)
        if not user.phone_number:
            raise RequirePhoneError
        return list(Favorite.objects.filter(user__chat_id=chat_id).values_list("favorite_user__username", flat=True))
