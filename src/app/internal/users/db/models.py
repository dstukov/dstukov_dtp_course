from django.contrib.auth.base_user import AbstractBaseUser, BaseUserManager
from django.contrib.auth.models import AbstractUser
from django.core.validators import RegexValidator
from django.db import models


class UserManager(BaseUserManager):
    def create_user(self, username: str, chat_id: int, password: str, **extra_fields):
        if not username:
            raise ValueError("The username must not be empty")
        user = self.model(username=username, chat_id=chat_id, **extra_fields)
        user.set_password(password)
        user.save()
        return user


class User(AbstractBaseUser):
    id = models.BigAutoField(primary_key=True, auto_created=True, serialize=False)
    chat_id = models.BigIntegerField()
    username = models.CharField(max_length=32, null=True, blank=True)
    first_name = models.CharField(max_length=256)
    last_name = models.CharField(max_length=256, null=True, blank=True)
    _phone_regex = RegexValidator(regex=r"^\+\d{8,15}$", message="Phone number format: '+999999999', <= 15 digits")
    phone_number = models.CharField(
        validators=[_phone_regex], max_length=16, null=True, default=None, blank=True, unique=True
    )
    is_superuser = models.BooleanField(default=False)
    is_blocked_bot = models.BooleanField(default=False)
    is_banned = models.BooleanField(default=False)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    objects = UserManager()

    def __str__(self):
        return f"@{self.username}" if self.username is not None else f"{self.id}"

    class Meta:
        verbose_name = "User"


class Favorite(models.Model):  # use many-to-many instead of that model inside User model
    id = models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name="id")
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="user")
    favorite_user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="favorite_user")

    objects: models.manager.BaseManager["Favorite"]


class AdminUser(AbstractUser):
    pass
