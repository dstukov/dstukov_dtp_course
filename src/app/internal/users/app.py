from ninja import NinjaAPI

from src.app.internal.users.db.repositories import FavoriteRepository, UserRepository
from src.app.internal.users.domain.services import FavoriteService, UserService
from src.app.internal.users.presentation.handlers import FavoriteHandlers, UserHandlers
from src.app.internal.users.presentation.routers import add_users_router


def configure_users_api(api: NinjaAPI):
    user_repo = UserRepository()
    user_service = UserService(user_repo)
    user_handlers = UserHandlers(user_service)
    favorite_repo = FavoriteRepository(user_repo)
    favorite_service = FavoriteService(user_repo, favorite_repo)
    favorite_handlers = FavoriteHandlers(favorite_service)
    add_users_router(api, user_handlers, favorite_handlers)
