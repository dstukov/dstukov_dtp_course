from django.core.files.base import ContentFile

from src.app.internal.postcards.db.models import Postcard


class PostcardRepository:
    def add_postcard(self, content: bytes) -> Postcard:
        image = ContentFile(content, "postcard")
        return Postcard.objects.create(image=image)

    def get_postcard_by_id(self, postcard_id: int) -> Postcard:
        return Postcard.objects.get(pk=postcard_id)
