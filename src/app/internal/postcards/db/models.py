from pathlib import Path
from uuid import UUID, uuid4

from django.db import models


def get_filename_to_upload(_, __):
    upload_to = "postcards"
    filename = Path(f"{UUID(str(uuid4())).hex}.png")
    return upload_to / filename


class Postcard(models.Model):
    id = models.BigAutoField(auto_created=True, primary_key=True)
    image = models.FileField(upload_to=get_filename_to_upload)
