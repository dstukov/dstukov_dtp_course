from django.contrib import admin

from src.app.internal.postcards.db.models import Postcard


@admin.register(Postcard)
class PostcardAdmin(admin.ModelAdmin):
    pass
