from django.contrib import admin

from src.app.internal.auth.presentation.admin import IssuedTokenAdmin
from src.app.internal.bank.presentation.admin import PaymentAccountAdmin, PaymentCardAdmin, TransactionAdmin
from src.app.internal.postcards.presentation.admin import PostcardAdmin
from src.app.internal.users.presentation.admin import AdminUserAdmin, FavoriteAdmin, UserAdmin

admin.site.site_title = "Backend course"
admin.site.site_header = "Backend course"
