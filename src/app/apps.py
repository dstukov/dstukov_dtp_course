from django.apps import AppConfig as Config


class AppConfig(Config):
    name = "src.app"
