# Generated by Django 4.1.6 on 2023-04-10 15:46

from django.db import migrations, models

import src.app.internal.bank.db.models


class Migration(migrations.Migration):
    dependencies = [
        ("app", "0006_alter_paymentaccount_id_alter_paymentcard_id_and_more"),
    ]

    operations = [
        migrations.AlterField(
            model_name="paymentcard",
            name="card_number",
            field=models.CharField(
                max_length=20,
                validators=[src.app.internal.bank.db.models.payment_card_validator],
            ),
        ),
    ]
