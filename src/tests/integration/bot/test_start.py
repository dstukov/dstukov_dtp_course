from unittest.mock import AsyncMock, Mock

import pytest

from src.app.internal.handlers.bot.handlers import send_start_reply, user_service


@pytest.mark.django_db
@pytest.mark.integration
@pytest.mark.asyncio
async def test_me_creates_user_without_phone():
    chat_id = 123
    user_mock = Mock(id=chat_id, username="Test", first_name="fname", last_name="lname")
    message_mock = AsyncMock(from_user=user_mock)
    await send_start_reply(message=message_mock)
    user = await user_service.get_by_chat_id(chat_id)

    assert user.phone_number is None


@pytest.mark.django_db
@pytest.mark.integration
@pytest.mark.asyncio
async def test_me_replies_different_when_user_have_phone_or_not():
    chat_id = 123
    user_mock = Mock(id=chat_id, username="Test", first_name="fname", last_name="lname")
    message_mock = AsyncMock(from_user=user_mock)
    await send_start_reply(message=message_mock)
    user_without_phone_reply = message_mock.answer()

    await user_service.set_phone(chat_id, "+78005553535")
    await send_start_reply(message=message_mock)

    assert message_mock.answer() != user_without_phone_reply
