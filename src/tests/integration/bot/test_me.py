from unittest.mock import AsyncMock, Mock

import pytest

from src.app.internal.handlers.bot.handlers import send_me_reply
from src.app.internal.utils.user_serializer import serialize_user_telegram
from src.tests.utils.user_factory import UserFactory


@pytest.mark.django_db
@pytest.mark.integration
@pytest.mark.asyncio
async def test_me():
    user = await UserFactory.create_user()
    user_mock = Mock(id=user.chat_id)
    message_mock = AsyncMock(from_user=user_mock)

    await send_me_reply(message=message_mock)

    message_mock.reply.assert_called_with(f"<b>Information about user:</b> {serialize_user_telegram(user)}")
