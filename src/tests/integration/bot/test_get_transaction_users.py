from unittest.mock import AsyncMock, Mock

import pytest

from src.app.internal.handlers.bot.handlers import payment_account_service, send_get_transaction_users_answer
from src.tests.utils.payment_account_factory import PaymentAccountFactory


@pytest.mark.django_db
@pytest.mark.integration
@pytest.mark.asyncio
async def test_get_transaction_users_should_return_empty_text_when_no_transactions():
    account = await PaymentAccountFactory.create_account(100)

    user_mock = Mock(id=account.user.chat_id)
    message_mock = AsyncMock(from_user=user_mock)

    await send_get_transaction_users_answer(message=message_mock)

    message_mock.answer.assert_called_with("No transactions found")


@pytest.mark.django_db
@pytest.mark.integration
@pytest.mark.asyncio
async def test_get_transaction_users_should_return_correct_usernames():
    sender = await PaymentAccountFactory.create_account(100)
    recipient = await PaymentAccountFactory.create_account(100)

    await payment_account_service.transfer_money(sender, recipient, 50)

    user_mock = Mock(id=sender.user.chat_id)
    message_mock = AsyncMock(from_user=user_mock)

    await send_get_transaction_users_answer(message=message_mock)

    message_mock.answer.assert_called_with(f"Transactions: {recipient.user.username}")
