import datetime
from unittest.mock import AsyncMock, Mock

import pytest
from django.utils import timezone

from src.app.internal.handlers.bot.handlers import (
    account_statement_service,
    payment_account_service,
    send_get_account_statement_answer,
)
from src.tests.utils.payment_account_factory import PaymentAccountFactory


@pytest.mark.django_db
@pytest.mark.integration
@pytest.mark.asyncio
async def test_get_account_statement_should_return_correct_statement():
    sender = await PaymentAccountFactory.create_account(100)
    recipient = await PaymentAccountFactory.create_account(100)

    await payment_account_service.transfer_money(sender, recipient, 50)

    statement = await account_statement_service.get_account_statement(
        sender.user.chat_id, timezone.now() - datetime.timedelta(hours=10)
    )

    user_mock = Mock(id=sender.user.chat_id)
    message_mock = AsyncMock(from_user=user_mock)

    await send_get_account_statement_answer(message=message_mock)

    assert len(statement.latest_transactions) == 1

    message_mock.answer.assert_called_with(f"Account statement:\n{str(statement)}")
