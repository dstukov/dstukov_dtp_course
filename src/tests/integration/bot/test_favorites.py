from unittest.mock import AsyncMock, Mock

import pytest

from src.app.internal.handlers.bot.handlers import (
    send_delete_favorite_reply,
    send_favorites_reply,
    send_set_favorite_reply,
)
from src.tests.utils.user_factory import UserFactory


@pytest.mark.django_db
@pytest.mark.integration
@pytest.mark.asyncio
async def test_favorites_empty_by_default():
    user = await UserFactory.create_user()
    user_mock = Mock(id=user.chat_id)
    message_mock = AsyncMock(from_user=user_mock)
    await send_favorites_reply(message=message_mock)

    message_mock.answer.assert_called_with("[]")


@pytest.mark.django_db
@pytest.mark.integration
@pytest.mark.asyncio
async def test_favorites_return_correct_favorite_users():
    [user1, user2, user3] = await UserFactory.create_users(3)

    user_mock = Mock(id=user1.chat_id)
    message_mock = AsyncMock(from_user=user_mock)
    command_mock_with_chat_id = Mock(args=f"{user2.chat_id}")
    await send_set_favorite_reply(message=message_mock, command=command_mock_with_chat_id)

    user_mock.id = user2.chat_id
    message_mock = AsyncMock(from_user=user_mock)
    command_mock_with_chat_id = Mock(args=f"{user3.chat_id}")
    await send_set_favorite_reply(message=message_mock, command=command_mock_with_chat_id)

    await send_favorites_reply(message=message_mock)

    message_mock.answer.assert_called_with(f"['{user3.username}']")


@pytest.mark.django_db
@pytest.mark.integration
@pytest.mark.asyncio
async def test_favorites_return_correct_favorite_users_after_delete():
    [user1, user2] = await UserFactory.create_users(2)

    user_mock = Mock(id=user1.chat_id)
    message_mock = AsyncMock(from_user=user_mock)
    command_mock_with_chat_id = Mock(args=f"{user2.chat_id}")
    await send_set_favorite_reply(message=message_mock, command=command_mock_with_chat_id)

    await send_favorites_reply(message=message_mock)
    message_mock.answer.assert_called_with(f"['{user2.username}']")

    await send_delete_favorite_reply(message=message_mock, command=command_mock_with_chat_id)

    await send_favorites_reply(message=message_mock)
    message_mock.answer.assert_called_with("[]")
