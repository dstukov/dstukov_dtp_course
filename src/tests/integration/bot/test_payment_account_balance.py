from unittest.mock import AsyncMock, Mock

import pytest

from src.app.internal.handlers.bot.handlers import send_payment_account_balance_reply
from src.tests.utils.payment_account_factory import PaymentAccountFactory
from src.tests.utils.payment_card_factory import PaymentCardFactory


@pytest.mark.django_db
@pytest.mark.integration
@pytest.mark.asyncio
async def test_payment_account_balance_correct():
    account = await PaymentAccountFactory.create_account(150)
    card = await PaymentCardFactory.create_payment_card(account)

    user_mock = Mock(id=account.user.chat_id)
    message_mock = AsyncMock(from_user=user_mock)

    await send_payment_account_balance_reply(message_mock)

    message_mock.answer.assert_called_with(f"Account balance: 150.00\n\nCards:\n{card.card_number}")
