from unittest.mock import AsyncMock, Mock

import pytest

from src.app.internal.handlers.bot.handlers import send_transfer_money_reply
from src.tests.utils.payment_account_factory import PaymentAccountFactory
from src.tests.utils.user_factory import UserFactory


@pytest.mark.django_db
@pytest.mark.integration
@pytest.mark.asyncio
async def test_transfer_money_fail_when_incorrect_arguments_count():
    message_mock = AsyncMock()
    command_mock = Mock(args="single_argument")
    state_mock = AsyncMock()

    await send_transfer_money_reply(message=message_mock, command=command_mock, state=state_mock)

    message_mock.answer.assert_called_with("Incorrect amount of arguments: 1. Should be 2")


@pytest.mark.django_db
@pytest.mark.integration
@pytest.mark.asyncio
async def test_transfer_money_fail_when_transfer_to_same_account():
    account = await PaymentAccountFactory.create_account(100)
    user_mock = Mock(id=account.user.chat_id)
    message_mock = AsyncMock(from_user=user_mock)
    command_mock = Mock(args=f"100 {account.user.chat_id}")
    state_mock = AsyncMock()

    await send_transfer_money_reply(message=message_mock, command=command_mock, state=state_mock)

    message_mock.reply.assert_called_with("Cant send money to same account")


@pytest.mark.django_db
@pytest.mark.integration
@pytest.mark.asyncio
async def test_transfer_money_fail_when_transfer_money_not_only_with_digits():
    message_mock = AsyncMock()
    command_mock = Mock(args="1money1 12345")
    state_mock = AsyncMock()

    await send_transfer_money_reply(message=message_mock, command=command_mock, state=state_mock)

    message_mock.reply.assert_called_with("Money should consists of digits, but found 1money1")


# @pytest.mark.django_db
# @pytest.mark.integration
# @pytest.mark.asyncio
# async def test_transfer_money_fail_when_transfer_from_card_not_accessed_by_user():
#     user = await UserFactory.create_user()
#     user_mock = Mock(id=user.chat_id)
#     message_mock = AsyncMock(from_user=user_mock)
#     command_mock = Mock(args="123 100 12345")
#
#     await send_transfer_money_reply(message=message_mock, command=command_mock)
#
#     message_mock.reply.assert_called_with("Card with id 123 not found for your user")


@pytest.mark.django_db
@pytest.mark.integration
@pytest.mark.asyncio
async def test_transfer_money_fail_when_transfer_from_account_with_not_enough_money():
    account = await PaymentAccountFactory.create_account(0)
    user_mock = Mock(id=account.user.chat_id)
    message_mock = AsyncMock(from_user=user_mock)
    money = 100
    command_mock = Mock(args=f"{money} 12345")
    state_mock = AsyncMock()

    await send_transfer_money_reply(message=message_mock, command=command_mock, state=state_mock)

    message_mock.reply.assert_called_with(f"Not enough money: need {money}.00 to success")


@pytest.mark.django_db
@pytest.mark.integration
@pytest.mark.asyncio
async def test_transfer_money_fail_when_transfer_to_not_existing_account_by_id():
    account = await PaymentAccountFactory.create_account(1000)
    user_mock = Mock(id=account.user.chat_id)
    message_mock = AsyncMock(from_user=user_mock)
    command_mock = Mock(args="50 12345")
    state_mock = AsyncMock()

    await send_transfer_money_reply(message=message_mock, command=command_mock, state=state_mock)

    message_mock.reply.assert_called_with("Account with id 12345 not found for another user")


@pytest.mark.django_db
@pytest.mark.integration
@pytest.mark.asyncio
async def test_transfer_money_fail_when_transfer_to_user_without_account():
    account = await PaymentAccountFactory.create_account(100)
    user = await UserFactory.create_user()
    user_mock = Mock(id=account.user.chat_id)
    message_mock = AsyncMock(from_user=user_mock)
    command_mock = Mock(args=f"50 {user.username}")
    state_mock = AsyncMock()

    await send_transfer_money_reply(message=message_mock, command=command_mock, state=state_mock)

    message_mock.reply.assert_called_with("Another user haven't any account")


@pytest.mark.django_db
@pytest.mark.integration
@pytest.mark.asyncio
async def test_transfer_money_fail_when_transfer_to_itself_by_name():
    account = await PaymentAccountFactory.create_account(100)
    user_mock = Mock(id=account.user.chat_id)
    message_mock = AsyncMock(from_user=user_mock)
    command_mock = Mock(args=f"50 {account.user.username}")
    state_mock = AsyncMock()

    await send_transfer_money_reply(message=message_mock, command=command_mock, state=state_mock)

    message_mock.reply.assert_called_with("Cant send money to itself by name")


@pytest.mark.django_db
@pytest.mark.integration
@pytest.mark.asyncio
async def test_transfer_money_success():
    account1 = await PaymentAccountFactory.create_account(100)
    account2 = await PaymentAccountFactory.create_account(100)

    user_mock = Mock(id=account1.user.chat_id)
    message_mock = AsyncMock(from_user=user_mock)
    command_mock = Mock(args=f"50 {account2.user.chat_id}")
    state_mock = AsyncMock()

    await send_transfer_money_reply(message=message_mock, command=command_mock, state=state_mock)

    message_mock.answer.assert_called_with(
        "Transfer success. You can add postcard by sending photo now or skip that step"
    )
