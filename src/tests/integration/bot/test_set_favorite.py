from unittest.mock import AsyncMock, Mock

import pytest

from src.app.internal.handlers.bot.handlers import send_set_favorite_reply
from src.tests.utils.user_factory import UserFactory


@pytest.mark.django_db
@pytest.mark.integration
@pytest.mark.asyncio
async def test_set_favorite_for_user_id():
    [user1, user2] = await UserFactory.create_users(2)
    user_mock = Mock(id=user1.chat_id)
    message_mock = AsyncMock(from_user=user_mock)
    command_mock_with_chat_id = Mock(args=f"{user2.chat_id}")
    await send_set_favorite_reply(message=message_mock, command=command_mock_with_chat_id)

    message_mock.answer.assert_called_with(f"Add user {user2.chat_id}")


@pytest.mark.django_db
@pytest.mark.integration
@pytest.mark.asyncio
async def test_set_favorite_for_username():
    [user1, user2] = await UserFactory.create_users(2)
    message_mock = AsyncMock(from_user=Mock(id=user1.chat_id))
    command_mock_with_username = Mock(args=f"{user2.username}")
    await send_set_favorite_reply(message=message_mock, command=command_mock_with_username)

    message_mock.answer.assert_called_with(f"Add user {user2.username}")
