import json
from multiprocessing.connection import Client

import pytest
from _decimal import Decimal
from asgiref.sync import sync_to_async

from src.tests.integration.rest.users.test_favorites_put import request_put_favorites
from src.tests.integration.rest.utils import bearer, create_authorized_user
from src.tests.utils.payment_account_factory import PaymentAccountFactory
from src.tests.utils.user_factory import UserFactory


@pytest.mark.django_db
@pytest.mark.smoke
@pytest.mark.asyncio
async def test_money_transfer_should_return_401_when_unauthorized(client: Client):
    data = {"from_account": "username", "to_account": "username", "money": 10}
    response = await request_post_transfer_money(client, data)

    assert response.status_code == 401


@pytest.mark.django_db
@pytest.mark.smoke
@pytest.mark.asyncio
async def test_money_transfer_should_return_400_when_have_not_phone(client: Client):
    user, access_token = await create_authorized_user(client, phone_number=None)
    data = {"from_account": 123, "to_account": 123, "money": 10}

    response = await request_post_transfer_money(client, data, **bearer(access_token))

    assert response.status_code == 400


@pytest.mark.django_db
@pytest.mark.smoke
@pytest.mark.asyncio
async def test_money_transfer__should_return_200_when_all_fine(client: Client):
    user, access_token = await create_authorized_user(client)
    from_account = await PaymentAccountFactory.create_account(1000, user)
    target = await UserFactory.create_other_user()
    to_account = await PaymentAccountFactory.create_account(10, target)
    data = {"from_account": from_account.id, "to_account": to_account.id, "money": Decimal(10)}

    await request_put_favorites(client, {"username": target.username}, **bearer(access_token))
    response = await request_post_transfer_money(client, data, **bearer(access_token))

    assert response.status_code == 200


async def request_post_transfer_money(client: Client, data, **kwargs):
    return await sync_to_async(client.post)("/api/money/transfer/", data, **kwargs, content_type="application/json")
