import json
from multiprocessing.connection import Client

from asgiref.sync import sync_to_async

from src.app.internal.users.db.models import User
from src.tests.utils.user_factory import UserFactory

LOGIN_URL = "/api/auth/login/"


def login(client: Client, user: User, password: str) -> (str, str):
    data = {"username": user.username, "password": password}
    login_response = client.post(LOGIN_URL, data, accept="application/json", content_type="application/json")
    login_content = json.loads(login_response.content.decode("utf-8"))
    return login_content["access_token"], login_content["refresh_token"]


async def create_authorized_user(client: Client, **kwargs):
    password = kwargs["password"] if "password" in kwargs else "test_passwd"
    user = await UserFactory.create_user(password=password, **kwargs)
    access_token, refresh_token = await sync_to_async(login)(client, user, password)
    return user, access_token


def bearer(access_token: str):
    return {"HTTP_AUTHORIZATION": f"Bearer {access_token}"}
