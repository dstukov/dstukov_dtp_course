import json
from multiprocessing.connection import Client

import pytest
from asgiref.sync import sync_to_async

from src.tests.utils.user_factory import UserFactory

AUTH_URL = "/api/auth/login/"


@pytest.mark.django_db
@pytest.mark.integration
@pytest.mark.parametrize(
    "data",
    [
        {},
        {"username": "user"},
        {"password": "user"},
    ],
)
def test_login_return_422_when_incorrect_model(client: Client, data):
    response = client.post(AUTH_URL, data, content_type="application/json")

    assert response.status_code == 422


@pytest.mark.django_db
@pytest.mark.integration
def test_login_return_401_when_user_not_exists(client: Client):
    data = {"username": "user", "password": "pswd"}

    response = client.post(AUTH_URL, data, content_type="application/json")

    assert response.status_code == 401


@pytest.mark.django_db
@pytest.mark.integration
@pytest.mark.asyncio
async def test_login_return_success_when_correct_data(client: Client):
    password = "pswd"
    user = await UserFactory.create_user(password=password)
    data = {"username": user.username, "password": password}

    response = await sync_to_async(client.post)(AUTH_URL, data, content_type="application/json")
    content = json.loads(response.content.decode("utf-8"))

    assert response.status_code == 200
    assert "access_token" in content
    assert "refresh_token" in content
