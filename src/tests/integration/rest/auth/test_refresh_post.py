import datetime
import json
from multiprocessing.connection import Client

import pytest
from asgiref.sync import sync_to_async
from django.utils import timezone
from freezegun import freeze_time

from src.app.internal.utils.utils import JWT_REFRESH_TOKEN_EXPIRED
from src.tests.integration.rest.utils import login
from src.tests.utils.user_factory import UserFactory

REFRESH_URL = "/api/auth/refresh/"


@pytest.mark.django_db
@pytest.mark.integration
@pytest.mark.parametrize("data", [{}, {"incorrect_data": ""}])
def test_refresh_should_return_422_when_incorrect_data(client: Client, data):
    response = client.post(REFRESH_URL, data, content_type="application/json")

    assert response.status_code == 422


@pytest.mark.django_db
@pytest.mark.integration
@pytest.mark.asyncio
async def test_refresh_should_return_400_when_use_revoked_refresh_token(client: Client):
    password = "pswd"
    user = await UserFactory.create_user(password=password)

    access_token, refresh_token = await sync_to_async(login)(client, user, password)
    await sync_to_async(login)(client, user, password)
    response = client.post(REFRESH_URL, {"refresh_token": refresh_token}, content_type="application/json")

    assert response.status_code == 400


@pytest.mark.django_db
@pytest.mark.integration
@pytest.mark.asyncio
async def test_refresh_should_return_400_when_refresh_token_expired(client: Client):
    with freeze_time(datetime.datetime.now()) as freezer:
        password = "pswd"
        user = await UserFactory.create_user(password=password)

        access_token, refresh_token = await sync_to_async(login)(client, user, password)
        delay = datetime.timedelta(seconds=JWT_REFRESH_TOKEN_EXPIRED) + timezone.timedelta(seconds=10)
        freezer.tick(delta=delay)
        response = client.post(REFRESH_URL, {"refresh_token": refresh_token}, content_type="application/json")

        assert response.status_code == 400


@pytest.mark.django_db
@pytest.mark.integration
@pytest.mark.asyncio
async def test_refresh_works(client: Client):
    password = "pswd"
    user = await UserFactory.create_user(password=password)

    access_token, refresh_token = await sync_to_async(login)(client, user, password)
    response = client.post(REFRESH_URL, {"refresh_token": refresh_token}, content_type="application/json")
    refresh_content = json.loads(response.content.decode("utf-8"))

    assert response.status_code == 200
    assert "refresh_token" in refresh_content
    assert "access_token" in refresh_content
