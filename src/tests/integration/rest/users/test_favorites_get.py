import json
from multiprocessing.connection import Client

import pytest
from asgiref.sync import sync_to_async

from src.tests.integration.rest.users.test_favorites_put import FAVORITE_URL, request_put_favorites
from src.tests.integration.rest.utils import bearer, create_authorized_user
from src.tests.utils.user_factory import UserFactory


@pytest.mark.django_db
@pytest.mark.smoke
@pytest.mark.asyncio
async def test_favorites_get_should_return_401_when_unauthorized(client: Client):
    response = await request_get_favorites(client)

    assert response.status_code == 401


@pytest.mark.django_db
@pytest.mark.smoke
@pytest.mark.asyncio
async def test_favorites_get_should_return_400_when_have_not_phone(client: Client):
    user, access_token = await create_authorized_user(client, phone_number=None)

    response = await request_get_favorites(client, **bearer(access_token))

    assert response.status_code == 400


@pytest.mark.django_db
@pytest.mark.smoke
@pytest.mark.asyncio
async def test_favorites_get_should_return_200_when_several_users(client: Client):
    user, access_token = await create_authorized_user(client)
    target1 = await UserFactory.create_other_user(user)
    target2 = await UserFactory.create_other_user(user, target1)

    await request_put_favorites(client, {"username": target1.username}, **bearer(access_token))
    await request_put_favorites(client, {"username": target2.username}, **bearer(access_token))
    response = await request_get_favorites(client, **bearer(access_token))

    assert json.loads(response.content.decode("utf8")) == {"favorites": [target1.username, target2.username]}
    assert response.status_code == 200


@pytest.mark.django_db
@pytest.mark.smoke
@pytest.mark.asyncio
async def test_favorites_get_should_return_200_when_single_user(client: Client):
    user, access_token = await create_authorized_user(client)

    response = await request_get_favorites(client, **bearer(access_token))

    assert response.status_code == 200


async def request_get_favorites(client: Client, **kwargs):
    return await sync_to_async(client.get)(FAVORITE_URL, **kwargs)
