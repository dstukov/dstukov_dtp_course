from multiprocessing.connection import Client

import pytest
from asgiref.sync import sync_to_async

from src.tests.integration.rest.users.test_favorites_put import FAVORITE_URL, request_put_favorites
from src.tests.integration.rest.utils import bearer, create_authorized_user
from src.tests.utils.user_factory import UserFactory


@pytest.mark.django_db
@pytest.mark.smoke
@pytest.mark.asyncio
async def test_favorites_delete_should_return_401_when_unauthorized(client: Client):
    data = {"username": "username"}

    response = await request_delete_favorites(client, data)

    assert response.status_code == 401


@pytest.mark.django_db
@pytest.mark.smoke
@pytest.mark.asyncio
async def test_favorites_delete_should_return_400_when_have_not_phone(client: Client):
    user, access_token = await create_authorized_user(client, phone_number=None)
    data = {"username": "username"}

    response = await request_delete_favorites(client, data, **bearer(access_token))

    assert response.status_code == 400


@pytest.mark.django_db
@pytest.mark.smoke
@pytest.mark.asyncio
async def test_favorites_delete_should_return_422_when_incorrect_schema(client: Client):
    user, access_token = await create_authorized_user(client)
    data = {}

    response = await request_delete_favorites(client, data, **bearer(access_token))

    assert response.status_code == 422


@pytest.mark.django_db
@pytest.mark.smoke
@pytest.mark.asyncio
async def test_favorites_delete_should_return_400_when_user_not_found(client: Client):
    user, access_token = await create_authorized_user(client)
    data = {"username": "username"}

    response = await request_delete_favorites(client, data, **bearer(access_token))

    assert response.status_code == 400


@pytest.mark.django_db
@pytest.mark.smoke
@pytest.mark.asyncio
async def test_favorites_delete_should_return_200_when_not_favorite(client: Client):
    user, access_token = await create_authorized_user(client)
    target = await UserFactory.create_other_user(user)
    data = {"username": target.username}

    response = await request_delete_favorites(client, data, **bearer(access_token))

    assert response.status_code == 200


@pytest.mark.django_db
@pytest.mark.smoke
@pytest.mark.asyncio
async def test_favorites_should_return_200_when_all_fine(client: Client):
    user, access_token = await create_authorized_user(client)
    target = await UserFactory.create_other_user(user)
    data = {"username": target.username}

    put_response = await request_put_favorites(client, data, **bearer(access_token))
    response = await request_delete_favorites(client, data, **bearer(access_token))

    assert put_response.status_code == 200
    assert response.status_code == 200


async def request_delete_favorites(client: Client, data, **kwargs):
    return await sync_to_async(client.delete)(FAVORITE_URL, data, **kwargs, content_type="application/json")
