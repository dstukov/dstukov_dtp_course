from multiprocessing.connection import Client

import pytest
from asgiref.sync import sync_to_async

from src.tests.integration.rest.utils import bearer, login
from src.tests.utils.user_factory import UserFactory

ME_URL = "/api/users/me/"


@pytest.mark.django_db
@pytest.mark.smoke
def test_me_should_return_401_when_unauthorized(client: Client):
    response = client.get(ME_URL)

    assert response.status_code == 401


@pytest.mark.django_db
@pytest.mark.smoke
@pytest.mark.asyncio
async def test_me_should_return_400_when_have_not_phone(client: Client):
    password = "test_passwd"
    user = await UserFactory.create_user(password=password, phone_number=None)
    access_token, refresh_token = await sync_to_async(login)(client, user, password)
    headers = bearer(access_token)

    response = await sync_to_async(client.get)(ME_URL, **headers)

    assert response.status_code == 400


@pytest.mark.django_db
@pytest.mark.smoke
@pytest.mark.asyncio
async def test_me_should_return_200_when_have_phone(client: Client):
    password = "test_passwd"
    user = await UserFactory.create_user(password=password)
    access_token, refresh_token = await sync_to_async(login)(client, user, password)
    headers = bearer(access_token)

    response = await sync_to_async(client.get)(ME_URL, **headers)

    assert response.status_code == 200
