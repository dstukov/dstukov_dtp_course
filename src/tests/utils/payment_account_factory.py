import random

from src.app.internal.bank.db.models import PaymentAccount
from src.app.internal.handlers.bot.handlers import payment_account_service
from src.app.internal.users.db.models import User
from src.tests.utils.user_factory import UserFactory


class PaymentAccountFactory:
    @staticmethod
    async def create_account(money: float, user: User | None = None) -> PaymentAccount:
        if user is None:
            user = await UserFactory.create_user()
        return await payment_account_service.create(random.randint(0, 1_000_000), user, money)
