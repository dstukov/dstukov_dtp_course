from src.app.internal.bank.db.models import PaymentAccount, PaymentCard
from src.app.internal.handlers.bot.handlers import payment_card_service


class PaymentCardFactory:
    @staticmethod
    async def create_payment_card(account: PaymentAccount) -> PaymentCard:
        return await payment_card_service.create(account, "374245455400126")
