import random
import uuid

from src.app.internal.handlers.bot.handlers import user_service
from src.app.internal.users.db.models import User

_DEFAULT_USERS = [
    dict(
        first_name="Dmitrii",
        last_name="Stukov",
        password="qwerty12345",
    ),
    dict(
        first_name="triiDmi",
        last_name="kovStu",
        password="qwerty12345",
    ),
    dict(
        first_name="Stutrii",
        last_name="Dmikov",
        password="qwerty12345",
    ),
]


class UserFactory:  # use fixtures instead of factories
    @staticmethod
    async def create_user(**kwargs) -> User:
        return await UserFactory._create_user(**(_DEFAULT_USERS[0] | kwargs))

    @staticmethod
    async def create_users(count) -> [User]:
        return [await UserFactory._create_user(**u) for u in _DEFAULT_USERS[:count]]

    @staticmethod
    async def generate_users():
        for u in _DEFAULT_USERS:
            yield await UserFactory._create_user(**u)

    @staticmethod
    async def _create_user(**kwargs) -> User:
        password = kwargs["password"]
        del kwargs["password"]
        user = User.objects.create_user(
            username=str(uuid.uuid4())[:32], chat_id=random.randint(0, 1_000_000), password=password, **kwargs
        )
        if "phone_number" in kwargs:
            phone_number = kwargs["phone_number"]
        else:
            phone_number = f"+7{random.randint(1000000000, 9999999999)}"
        user = await user_service.set_phone(user.chat_id, phone_number)

        return user

    @staticmethod
    async def create_other_user(*users):
        for data in _DEFAULT_USERS:
            return await UserFactory.create_user(**data)
