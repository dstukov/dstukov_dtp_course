from src.app.internal.handlers.bot.handlers import favorite_service


class FavoriteFactory:
    @staticmethod
    def create_favorite(user_id: int, favorite_user_id: int):
        return favorite_service.create_favorite(user_id, favorite_user_id)
