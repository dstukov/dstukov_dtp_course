import pytest as pytest
import requests

from src.config.bot import TG_TOKEN


@pytest.mark.smoke
def test_bot_works():
    url = f"https://api.telegram.org/bot{TG_TOKEN}/getMe"
    response = requests.get(url)
    assert response.status_code == 200
