from multiprocessing.connection import Client

import pytest


@pytest.mark.django_db
@pytest.mark.smoke
def test_me_works(client: Client):
    response = client.get("/api/users/me/")

    assert response.status_code == 401
