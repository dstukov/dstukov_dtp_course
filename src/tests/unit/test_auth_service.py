from datetime import datetime, timedelta

import pytest
from django.utils import timezone
from freezegun import freeze_time

from src.app.internal.auth.db.models import IssuedToken
from src.app.internal.handlers.bot.handlers import auth_service
from src.app.internal.utils.exceptions import (
    RefreshTokenExpiredError,
    RefreshTokenHasBeenRevokedError,
    RefreshTokenNotRecognizedError,
    UserNotFoundError,
)
from src.app.internal.utils.utils import JWT_REFRESH_TOKEN_EXPIRED, generate_refresh_token, try_parse_token
from src.tests.utils.user_factory import UserFactory


@pytest.mark.django_db
@pytest.mark.unit
@pytest.mark.asyncio
async def test_login_should_raise_error_when_user_not_exists():
    with pytest.raises(UserNotFoundError):
        await auth_service.login("not_exist_user", "pswd")


@pytest.mark.django_db
@pytest.mark.unit
@pytest.mark.asyncio
async def test_login_should_raise_error_when_incorrect_old_password():
    password = "pswd"
    user = await UserFactory.create_user(password=password)

    with pytest.raises(UserNotFoundError):
        await auth_service.login(user.username, password + "a")


@pytest.mark.django_db
@pytest.mark.unit
@pytest.mark.asyncio
async def test_login_should_create_issued_token_when_correct_password():
    password = "pswd"
    user = await UserFactory.create_user(password=password)

    access_token, refresh_token = await auth_service.login(user.username, password)

    assert IssuedToken.objects.filter(user=user, revoked=False).count() == 1
    assert access_token
    assert refresh_token


@pytest.mark.django_db
@pytest.mark.unit
@pytest.mark.asyncio
async def test_login_lots_of_times_should_revoke_previous_tokens_when_correct_password():
    password = "pswd"
    user = await UserFactory.create_user(password=password)

    await auth_service.login(user.username, password)
    await auth_service.login(user.username, password)
    access_token, refresh_token = await auth_service.login(user.username, password)

    assert IssuedToken.objects.filter(user=user, revoked=True).count() == 2
    assert IssuedToken.objects.filter(user=user, revoked=False).count() == 1
    assert access_token
    assert refresh_token


@pytest.mark.django_db
@pytest.mark.unit
def test_refresh_should_raise_error_when_refresh_token_none():
    with pytest.raises(RefreshTokenNotRecognizedError):
        auth_service.refresh(None)


@pytest.mark.django_db
@pytest.mark.unit
def test_refresh_should_raise_error_when_refresh_token_incorrect():
    with pytest.raises(RefreshTokenNotRecognizedError):
        auth_service.refresh("incorrect_refresh_token")


@pytest.mark.django_db
@pytest.mark.unit
def test_refresh_should_raise_error_when_token_not_found():
    refresh_token = generate_refresh_token(0, timezone.timedelta(seconds=100))

    with pytest.raises(RefreshTokenNotRecognizedError):
        auth_service.refresh(refresh_token)


@pytest.mark.django_db
@pytest.mark.unit
@pytest.mark.asyncio
async def test_refresh_should_be_correct_when_refresh_token_expired():
    password = "pswd"
    user = await UserFactory.create_user(password=password)

    user_login = await auth_service.login(user.username, password)
    with freeze_time(datetime.now()) as freezer:
        delay = timedelta(seconds=JWT_REFRESH_TOKEN_EXPIRED) + timedelta(seconds=10)
        freezer.tick(delta=delay)
        with pytest.raises(RefreshTokenExpiredError):
            auth_service.refresh(user_login.refresh_token)

    assert not IssuedToken.objects.filter(user=user, revoked=False).exists()


@pytest.mark.django_db
@pytest.mark.unit
@pytest.mark.asyncio
async def test_refresh_should_be_correct_when_refresh_token_correct():
    password = "pswd"
    user = await UserFactory.create_user(password=password)

    schema = await auth_service.login(user.username, password)
    auth_service.refresh(schema.refresh_token)

    success, token = try_parse_token(schema.refresh_token)
    assert success
    assert IssuedToken.objects.filter(user=user, revoked=False).count() == 1
    assert IssuedToken.objects.filter(id=token["id"]).get().revoked


@pytest.mark.django_db
@pytest.mark.unit
@pytest.mark.asyncio
async def test_refresh_should_be_correct_when_refresh_token_revoked():
    password = "pswd"
    user = await UserFactory.create_user(password=password)

    old_login = await auth_service.login(user.username, password)
    await auth_service.login(user.username, password)

    with pytest.raises(RefreshTokenHasBeenRevokedError):
        auth_service.refresh(old_login.refresh_token)

    assert not IssuedToken.objects.filter(user=user, revoked=False).exists()
