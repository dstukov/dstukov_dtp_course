import pytest
from _decimal import Decimal

from src.app.internal.bank.db.models import PaymentAccount
from src.app.internal.handlers.bot.handlers import payment_account_service, transaction_service
from src.tests.utils.payment_account_factory import PaymentAccountFactory
from src.tests.utils.user_factory import UserFactory


@pytest.mark.django_db
@pytest.mark.unit
@pytest.mark.asyncio
async def test_get_payment_account_raise_error_when_account_id_not_exists():
    with pytest.raises(PaymentAccount.DoesNotExist):
        not_exists_id = 100500
        await payment_account_service.get_by_id(not_exists_id)


@pytest.mark.django_db
@pytest.mark.unit
@pytest.mark.asyncio
async def test_create_payment_account_should_save_initial_data():
    user = await UserFactory.create_user()
    account_id = 1
    money = Decimal(100)
    account = await payment_account_service.create(account_id, user, money)

    assert account.id == account_id
    assert account.user == user
    assert account.money == money


@pytest.mark.django_db
@pytest.mark.unit
@pytest.mark.asyncio
async def test_should_transfer_money():
    account1 = await PaymentAccountFactory.create_account(100)
    account2 = await PaymentAccountFactory.create_account(100)

    await payment_account_service.transfer_money(account1, account2, Decimal(50))

    account1 = await payment_account_service.get_by_id(account1.id)
    account2 = await payment_account_service.get_by_id(account2.id)

    assert account1.money == 50
    assert account2.money == 150


@pytest.mark.django_db
@pytest.mark.unit
@pytest.mark.asyncio
async def test_should_create_transaction():
    sender = await PaymentAccountFactory.create_account(100)
    recipient = await PaymentAccountFactory.create_account(100)

    assert len(await transaction_service.get_transaction_history(sender.user.chat_id)) == 0

    await payment_account_service.transfer_money(sender, recipient, Decimal(50))

    assert len(await transaction_service.get_transaction_history(sender.user.chat_id)) == 1
