import datetime

import pytest
from _decimal import Decimal
from django.utils import timezone
from freezegun import freeze_time

from src.app.internal.handlers.bot.handlers import transaction_service
from src.tests.utils.payment_account_factory import PaymentAccountFactory


@pytest.mark.django_db
@pytest.mark.unit
@pytest.mark.asyncio
async def test_create_should_save_initial_data():
    date = timezone.now()

    with freeze_time(date):
        sender = await PaymentAccountFactory.create_account(100)
        recipient = await PaymentAccountFactory.create_account(100)

        transaction = await transaction_service.create(sender, recipient, Decimal(50))

        assert transaction.money == Decimal(50)
        assert transaction.recipient_account == recipient
        assert transaction.sender_account == sender
        assert transaction.date == date


@pytest.mark.django_db
@pytest.mark.unit
@pytest.mark.asyncio
async def test_get_recipients_usernames_should_return_correct_usernames():
    sender = await PaymentAccountFactory.create_account(100)
    recipient = await PaymentAccountFactory.create_account(100)

    await transaction_service.create(sender, recipient, Decimal(50))

    usernames_for_sender = await transaction_service.get_recipients_usernames(sender.user.chat_id)
    usernames_for_recipient = await transaction_service.get_recipients_usernames(recipient.user.chat_id)

    assert len(usernames_for_sender) == 1
    assert len(usernames_for_recipient) == 0


@pytest.mark.django_db
@pytest.mark.unit
@pytest.mark.asyncio
async def test_get_senders_usernames_should_return_correct_usernames():
    sender = await PaymentAccountFactory.create_account(100)
    recipient = await PaymentAccountFactory.create_account(100)

    await transaction_service.create(sender, recipient, Decimal(50))

    usernames_for_sender = await transaction_service.get_senders_usernames(sender.user.chat_id)
    usernames_for_recipient = await transaction_service.get_senders_usernames(recipient.user.chat_id)

    assert len(usernames_for_sender) == 0
    assert len(usernames_for_recipient) == 1


@pytest.mark.django_db
@pytest.mark.unit
@pytest.mark.asyncio
async def test_get_transaction_history_should_return_correct_usernames():
    account1 = await PaymentAccountFactory.create_account(100)
    account2 = await PaymentAccountFactory.create_account(100)
    account3 = await PaymentAccountFactory.create_account(100)

    await transaction_service.create(account1, account2, Decimal(50))
    await transaction_service.create(account2, account3, Decimal(50))

    assert len(await transaction_service.get_transaction_history(account1.user.chat_id)) == 1
    assert len(await transaction_service.get_transaction_history(account2.user.chat_id)) == 2
    assert len(await transaction_service.get_transaction_history(account3.user.chat_id)) == 1


@pytest.mark.django_db
@pytest.mark.unit
@pytest.mark.asyncio
async def test_get_last_transactions_should_return_correct_transactions():
    date = timezone.now()

    with freeze_time(date):
        sender = await PaymentAccountFactory.create_account(100)
        recipient = await PaymentAccountFactory.create_account(100)

        await transaction_service.create(sender, recipient, Decimal(50))

        transactions = await transaction_service.get_last_transactions(sender.id, date)

        assert len(transactions) == 1

        transactions = await transaction_service.get_last_transactions(sender.id, date + datetime.timedelta(hours=10))

        assert len(transactions) == 0
