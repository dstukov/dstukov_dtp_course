import pytest

from src.app.internal.middlewares.phone_required_middleware import user_service
from src.app.internal.users.db.models import User
from src.app.internal.utils.exceptions import IncorrectUserPasswordError
from src.tests.utils.user_factory import UserFactory


@pytest.mark.django_db
@pytest.mark.unit
@pytest.mark.asyncio
async def test_get_by_chat_id_raise_error_when_user_id_not_exists():
    with pytest.raises(User.DoesNotExist):
        not_exists_id = 100500
        await user_service.get_by_chat_id(not_exists_id)


@pytest.mark.django_db
@pytest.mark.unit
@pytest.mark.asyncio
async def test_get_by_username_raise_error_when_username_not_exists():
    with pytest.raises(User.DoesNotExist):
        not_exists_username = "not exists"
        await user_service.get_by_username(not_exists_username)


@pytest.mark.django_db
@pytest.mark.unit
@pytest.mark.asyncio
async def test_register_bot_user_should_save_initial_data():
    chat_id = 123456
    username = "save us please"
    first_name = "Babama"
    last_name = "Mamaba"

    [user, _] = await user_service.register_bot_user(chat_id, username, first_name, last_name)

    assert user.chat_id == chat_id
    assert user.username == username
    assert user.first_name == first_name
    assert user.last_name == last_name


@pytest.mark.django_db
@pytest.mark.unit
@pytest.mark.asyncio
async def test_register_bot_user_should_return_created_when_create_same_user():
    chat_id = 23456
    username = "not save us please"
    first_name = "Mamaba"
    last_name = "Babama"

    [_, result] = await user_service.register_bot_user(chat_id, username, first_name, last_name)

    assert result is True

    [_, result] = await user_service.register_bot_user(chat_id, username, first_name, last_name)

    assert result is False


@pytest.mark.django_db
@pytest.mark.unit
@pytest.mark.asyncio
async def test_set_phone_should_save_phone_number():
    user = await UserFactory.create_user()
    phone_number = +78005553535
    user = await user_service.set_phone(user.chat_id, phone_number)
    assert user.phone_number == phone_number


@pytest.mark.django_db
@pytest.mark.unit
@pytest.mark.asyncio
async def test_update_password_should_raise_error_when_user_not_exists():
    with pytest.raises(User.DoesNotExist):
        await user_service.update_password(100500, "a", "b")


@pytest.mark.django_db
@pytest.mark.unit
@pytest.mark.asyncio
async def test_update_password_should_raise_error_when_wrong_password():
    password = "pswd"
    user = await UserFactory.create_user(password=password)

    with pytest.raises(IncorrectUserPasswordError):
        await user_service.update_password(user.chat_id, password + "a", password + "b")


@pytest.mark.django_db
@pytest.mark.unit
@pytest.mark.asyncio
async def test_update_password_should_be_correct_when_correct_password():
    password = "pswd"
    new_password = "newpassword"
    user = await UserFactory.create_user(password=password)

    await user_service.update_password(user.chat_id, new_password, password)

    assert User.objects.filter(id=user.id).get().check_password(new_password)
