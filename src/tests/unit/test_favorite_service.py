import pytest

from src.app.internal.handlers.bot.handlers import favorite_service
from src.app.internal.users.db.models import Favorite
from src.tests.utils.user_factory import UserFactory


@pytest.mark.django_db
@pytest.mark.unit
@pytest.mark.asyncio
async def test_add_favorite_by_id_raise_error_when_set_favorite_by_itself():
    user = await UserFactory.create_user()
    with pytest.raises(Exception):
        await favorite_service.add_favorite_by_chat_id(user.chat_id, user.chat_id)


@pytest.mark.django_db
@pytest.mark.unit
@pytest.mark.asyncio
async def test_add_favorite_by_id_raise_error_when_set_favorite_to_already_favorite():
    [user1, user2] = await UserFactory.create_users(2)
    await favorite_service.add_favorite_by_chat_id(user1.chat_id, user2.chat_id)
    with pytest.raises(Exception):
        await favorite_service.add_favorite_by_chat_id(user1.chat_id, user2.chat_id)


@pytest.mark.django_db
@pytest.mark.unit
@pytest.mark.asyncio
async def test_add_favorite_by_id_raise_error_when_initial_user_not_exists():
    with pytest.raises(Exception):
        await favorite_service.add_favorite_by_chat_id(123, 234)


@pytest.mark.django_db
@pytest.mark.unit
@pytest.mark.asyncio
async def test_add_favorite_by_id_should_save_initial_data():
    [user1, user2] = await UserFactory.create_users(2)
    favorite = await favorite_service.add_favorite_by_chat_id(user1.chat_id, user2.chat_id)
    assert favorite.user == user1
    assert favorite.favorite_user == user2


@pytest.mark.django_db
@pytest.mark.unit
@pytest.mark.asyncio
async def test_add_favorite_by_username_should_save_initial_data():
    [user1, user2] = await UserFactory.create_users(2)
    favorite = await favorite_service.add_favorite_by_username(user1.chat_id, user2.username)
    assert favorite.user == user1
    assert favorite.favorite_user == user2


@pytest.mark.django_db
@pytest.mark.unit
@pytest.mark.asyncio
async def test_get_favorite_for_should_be_successful():
    [user1, user2] = await UserFactory.create_users(2)
    favorite = await favorite_service.add_favorite_by_username(user1.chat_id, user2.username)
    favorite_result = await favorite_service.get_favorite_for(user1.chat_id, user2.chat_id)
    assert favorite_result == favorite


@pytest.mark.django_db
@pytest.mark.unit
@pytest.mark.asyncio
async def test_remove_favorite_by_username_should_be_successful():
    [user1, user2] = await UserFactory.create_users(2)
    await favorite_service.add_favorite_by_username(user1.chat_id, user2.username)
    await favorite_service.remove_favorite_by_username(user1.chat_id, user2.username)

    with pytest.raises(Favorite.DoesNotExist):
        await favorite_service.get_favorite_for(user1.chat_id, user2.chat_id)


@pytest.mark.django_db
@pytest.mark.unit
@pytest.mark.asyncio
async def test_remove_favorite_by_id_should_be_successful():
    [user1, user2] = await UserFactory.create_users(2)
    await favorite_service.add_favorite_by_username(user1.chat_id, user2.username)
    await favorite_service.remove_favorite_by_id(user1.chat_id, user2.chat_id)

    with pytest.raises(Favorite.DoesNotExist):
        await favorite_service.get_favorite_for(user1.chat_id, user2.chat_id)


@pytest.mark.django_db
@pytest.mark.unit
@pytest.mark.asyncio
async def test_is_favorite_for_should_be_true_when_user_favorite_to_another():
    [user1, user2] = await UserFactory.create_users(2)
    await favorite_service.add_favorite_by_username(user1.chat_id, user2.username)
    result = await favorite_service.is_favorite_for(user1.chat_id, user2.chat_id)
    assert result is True


@pytest.mark.django_db
@pytest.mark.unit
@pytest.mark.asyncio
async def test_is_favorite_for_should_be_false_when_user_not_favorite_to_another():
    [user1, user2] = await UserFactory.create_users(2)
    await favorite_service.add_favorite_by_username(user1.chat_id, user2.username)
    result = await favorite_service.is_favorite_for(user2.chat_id, user1.chat_id)
    assert result is False
