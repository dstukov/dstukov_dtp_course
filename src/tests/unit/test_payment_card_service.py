import pytest
from django.db import DataError

from src.app.internal.bank.db.models import PaymentCard
from src.app.internal.handlers.bot.handlers import payment_card_service
from src.tests.utils.payment_account_factory import PaymentAccountFactory


@pytest.mark.django_db
@pytest.mark.unit
@pytest.mark.asyncio
async def test_get_payment_card_raise_error_when_card_id_not_exists():
    with pytest.raises(PaymentCard.DoesNotExist):
        not_exists_id = 100500
        await payment_card_service.get_by_id(not_exists_id)


@pytest.mark.django_db
@pytest.mark.unit
@pytest.mark.asyncio
async def test_create_payment_card_should_save_initial_data():
    account = await PaymentAccountFactory.create_account(100)
    valid_card_number = "374245455400126"
    card = await payment_card_service.create(account, valid_card_number)
    assert card.payment_account_id == account
    assert card.card_number == valid_card_number


@pytest.mark.django_db
@pytest.mark.unit
@pytest.mark.asyncio
async def test_create_payment_card_should_validate_card_number():
    with pytest.raises(DataError):
        account = await PaymentAccountFactory.create_account(100)
        invalid_card_number = "374245455400127090990"
        await payment_card_service.create(account, invalid_card_number)
