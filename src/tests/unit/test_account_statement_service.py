import pytest
from _decimal import Decimal
from django.utils import timezone
from freezegun import freeze_time

from src.app.internal.handlers.bot.handlers import account_statement_service, payment_account_service
from src.tests.utils.payment_account_factory import PaymentAccountFactory
from src.tests.utils.payment_card_factory import PaymentCardFactory


@pytest.mark.django_db
@pytest.mark.unit
@pytest.mark.asyncio
async def test_get_account_statement_should_be_successful():
    date = timezone.now()

    with freeze_time(date):
        account1 = await PaymentAccountFactory.create_account(100)
        account2 = await PaymentAccountFactory.create_account(100)
        card = await PaymentCardFactory.create_payment_card(account1)

        await payment_account_service.transfer_money(account1, account2, Decimal(50))

        statement = await account_statement_service.get_account_statement(account1.user.chat_id, date)

        assert statement.payment_account == account1
        assert len(statement.card_numbers) == 1
        assert statement.card_numbers[0] == card.card_number
        assert len(statement.latest_transactions) == 1
        assert statement.latest_transactions[0].money == 50
        assert statement.latest_transactions[0].date == date
        assert statement.latest_transactions[0].sender_username == account1.user.username
        assert statement.latest_transactions[0].recipient_username == account2.user.username
